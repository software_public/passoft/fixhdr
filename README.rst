======
fixhdr
======

* Description: Modifies mseed headers

* Usage: fixhdr
         fixhdr -#
         fixhdr -h
         fixhdr [-d DataDirs] [-m batch_file] [-t batch_file] [-e endianess]

* Free software: GNU General Public License v3 (GPLv3)
