#!/usr/bin/env python
"""
# gui to aid in modifying mseed headers
# author: bcb
#
##########################
# modification
# author: bcb
# date 2003.304
# modified to use struct instead of array when reading traces
# removed reliance on mseedhdr. now self-contained.
#
##########################
# modification
# author: bcb
# date 2003.308
# bug fix: SetHdrVars were not being cleared after BuildTrcList
#
##########################
# modification
# author: bcb
# date 2003.309
#
# modified to use LibTrace
#
##########################
# modification
# author: bcb
# date 2003.321
#
# openned global modify to allow for wildcarding all 'Matching Headers'
#
##########################
# modification
# author: bcb
# date 2003.339
#
# added log notebook and ability to save
# added ability to load and save templates of changes
# added error handling when finding files to log exceptions
#        to log notebook if directories are unreadable or files are not
#        read/write by uid of process
#
##########################
# modification
# author: bcb
# date 2005.027
#
# added try/except for directory & file open in self.FindTrace to
# reflect changes in LibTrace.py
# Optimized FindTrace
#
##########################
# modification
# author: bcb
# date 2005.040
#
# modified to handle endianess of header when writing
# added button to list files for given Sta:Chan:Loc:Net:Sps identifier
# correctly handles mseed files that are corrupt
#
##########################
# modification
# author: bcb
# date 2005.143
#
# Added two new notebooks/functionalities
#        Time Shift notebook
#                allows user to apply bulk time shifts keyed to Stat:Loc:Net
#                associated functions:
#                         buildTimeShift
#                         UpdateTimeShift
#                         TimeSet
#                         ApplyTimeCor
#                         FillTime
#                         ClearAllTime
#                        SetFlag
#
#        Endianess notebook
#                allows user to switch endianess of data headers
#                associated functions:
#                         buildEndian
#                         ChangeEndian
#
#        Added features:
#
#                Wait utility
#                        if something is running pops-up a window telling user
#                        to wait
#                        functions:
#                                WaitWidget
#                Warn utility
#                         notifies user if headers have been changed
#                         functions:
#                                 WarnWidget
#                                 Ignore
#                 Enhanced Logging functionality
#                         user can now sort log on All, Headers, Time, Endian,
#                         or Summary
#                         user can flush logs
#                         functions:
#                                 FlushLogs
#                                 DisplayLogs
#
##########################
# modification
# author: bcb
# date 2005.252
#
# fixed round-off error of microseconds in self.FillTime
# Added to TimeShift notebook the ability to set/unset the Data Quality flag in
# the FSDH that indicates the "Time tag is questionable"
# Updated help & man page
# implemented sorts for displaying several dictionary's contents
##########################
# modification
# author: bcb
# date 2005.255
#
# Optimization of FindTrace
# added GetStartEnd

##########################
# modification
# author: bcb
# date 2005.279
#
# changed format of template to be easier to maintian by hand
# introduced batchmode (cmdline -b option)
# now importing getopt to parse commandline args
# changed MSEED format tab to conform to SEED Manual v2.3, Feb 1993, Appendix A
# threaded all operations that write to headers so that they can be cleanly
# stopped

##########################
# modification
# author: bcb
# date 2005.283
#
# fixed bug in LaunchFindTrace that tried to build a gui in batchmode

##########################
# modification
# author: bcb
# date 2005.300
#
# bug fix in BuildEpoch in try/except that revealed itself if time format was
# incorrect.
# add ResetTimeShift
# add CheckTimeFormat to ensure proper Time format entered
#
##########################
# modification
# author: bcb
# date 2005.307
#
# bug fix in ApplyTimeCor that caused not all traces to be corrected
# Changed granularity of selection times from milliseconds to seconds to
# avoid roundoff errors going from string to epoch and back again.
# Added feature in Time Shift window allowing user to wildcard location codes
#
##########################
# modification
# author: bcb
# date 2006.075
#
# replaced thread method isAlive with join after reports from user
# that isAlive was excessively slow
#
##########################
# modification
# author: bcb
# date 2007.133
#
# new stuff-
# new flags, batchmode for time & endian, template for time corrections,
# better handling of previous header or time corrections, undo time correction
# button, record of time corrections, changed method of handling batchmode,
# allows user to specify datadir, list time corrections
#
# new methods
#        TimeDictSet, WildCardLoc, NewOnlyTime, LoadTimeSelect,
#        DumpUpdateDicts, Usage, ExitWidget, GetTimeCor, ListTimeCor, AddZero,
#        LaunchUndoTimeCor, CancelWarnTL, RecalcStartEnd
#
# reworked methods
#        LaunchApplyTimeCor, ApplyTimeCor, UpdateTimeShift, GetStartEnd,
#        writeFile, LoadTemplate, ExitCheck, wait, WriteLog, addTextInfoBar
#
# removed methods
#        WaitWidget, ModAndExit
#
# fix bug whereby 'Save Header Template' ignored current entries on 'Trace
# Header' tab
##########################
# modification
# author: bcb
# date 2007.177
#
# bug fix in LoadTemplate
#        when loading timelist if shift = NA or na then shift was left
#        unassigned this caused Time_Shift_sec to be an invalid float later
#        in processing
##########################
# modification
# author: bcb
# date 2007.192
#
# changed reporting of rw errors to just listing number of files w/o rw
# permission
# added column to ListTimeCor to display if 'Timing Questionable' flag is set
# or unset
# added balloon help
# added Template format help
##########################
# modification
# author: bcb
# date 2007.342
#
# reworked method: TimeSet to incorporate TestTimeShift
# added method: TestTimeShift to ensure user doesn't exceed time correction
# field
#
##########################
# modification
# author: bcb
# date 2008:154
#
# bug fix: In ApplyTimeCor missed instance when blk_start < startTime < blk_end
# and blk_start< endTime<blk_end
################################################################
#
# modification
# version: 2008.219
# author: Bruce Beaudoin
#
# modified to take advantage of LibTrace optimization
# cleaned up testing of files. Now done in FindTrace not everytime file is
# opened.
# added globbing to Directory path
# optimized several routines with generators (thanks Lloyd)
################################################################
#
# modification
# version: 2009.175
# author: Jayson Barr
#
# modified to no longer create threads.  I commented out all the lines with
# threading calls so this can later be noted where would be useful to add it
# in again.  The reason for this is because everytime a thread was used it was
# defined, started and then joined immediately.  Therefore, there was no reason
# for them to be threads at all, and it was breaking with Tk because the
# functions that were the targets for the threads weren't actually Tk/thread
# safe.
################################################################
#
# modification
# version: 2018.176
# author: Derick Hess
# Updated cookiecutter version, added call to main(), etc.
#
################################################################
#
# modification
# version: 2019.025
# author: Lan Dam
# convert to py3
# bug fix: In GetStartEnd when no "Stat:Loc:Net" selected (timeid="")
################################################################
#
# modification
# version: 2020.212
# author: Maeva Pourpoint
#
# Updates to work under Python 3.
# Unit tests to ensure basic functionality.
# Code cleanup to conform to the PEP8 style guide.
# Directory cleanup (remove unused files introduced by Cookiecutter).
# Packaged with conda.
################################################################
#
# modification
# version: 2022.1.0.0
# author: Maeva Pourpoint
#
# New versioning scheme
################################################################
#
# modification
# version: 2023.3.0.0
# author: Destiny Kuehn
#
# Changed gui from Tkinter to PySide2
# Removed Pmw dependency
# Updated version number
# Put the initialization of important variables/lists/dicts
#   that were in the main class's constructor into two separate
#   methods, initBatchVars and initGuiVars, for batchmode and the
#   gui respectively
# Added child class TopLevel to handle creation of all child windows
#  (sans Progress Window for threads and List Time Corrections window)
# Variable names are unchanged unless they signified the widget type:
#   ie, self.DataDirs_e for the tkinter Entry widget was changed to
#   self.DataDirs_le for the PySide LineEdit widget
# Added a new button in Global Modify to clear location codes,
#   and updated Help tab with button usage
# Implemented heavy functions to run as threads
# Updated python version support to 3.9, 3.10, and 3.11
################################################################
#
# modification
# version: 2024.4.0.0
# author: Omid Hosseini
#
# Changed from PySide2 to PySide6
"""

import os
import subprocess
import sys
import time
from getopt import getopt
from glob import glob
from operator import mod
from PySide6.QtWidgets import (QApplication, QWidget, QComboBox, QLineEdit,
                               QCheckBox, QRadioButton, QPushButton,
                               QStackedWidget, QTabWidget, QVBoxLayout,
                               QHBoxLayout, QGridLayout, QGroupBox,
                               QLabel, QFormLayout, QSizePolicy,
                               QSpacerItem, QTextEdit, QFileDialog,
                               QMessageBox, QProgressDialog, QTreeView,
                               QAbstractItemView, QTableWidget,
                               QTableWidgetItem)
from PySide6.QtGui import (QColor, QBrush, QTextCursor, QStandardItemModel,
                           QStandardItem)
from PySide6.QtCore import (Qt, QThread, QObject, Signal, QEventLoop, QTimer)
from fixhdr.LibTrace import *

VERSION = "2024.4.0.0"
SPACE = " "
DATALOC = ""

# vars used for batchmode
BatchFile = ""
Endianess = ""
RunHeaderBatch = 0
RunTimeBatch = 0
RunEndianBatch = 0
BATCHMODE = 0


def Usage(err=''):
    print(err)
    print("Usage\nfixhdr \nfixhdr -#\nfixhdr -h\nfixhdr [-d DataDirs] "
          "[-m batch_file] [-t batch_file] [-e endianess]")
    print("\n       Invoked with no arguements, fixhdr will launch the GUI\n")
    print("       -d DataDirs - colon separated list of data directories "
          "[default: cwd]")
    print("       -e endianess - convert trace headers to endianess ["
          "big or little]")
    print("       -h - this usage page")
    print("       -m batch_file - correct mseed headers in batch mode using "
          "batch_file")
    print("       -t batch_file - correct trace timing in batch mode using "
          "batch_file")
    print("       -# - print version number")
    print("      NOTE: -m, -t & -e are mutually exclusive. If you wish to do")
    print("             both timing and headers, run time corrections first.")


# get commandline args
try:
    opts, pargs = getopt(sys.argv[1:], 'hd:t:m:e:#')
except Exception:
    err = "\nERROR: Invalid command line usage\n"
    Usage(err)
    sys.exit(1)
else:
    for flag, arg in opts:
        if flag == "-h":
            Usage()
            sys.exit(0)
        if flag == "-d":
            DATALOC = arg
        if flag == "-e":
            Endianess = arg
            if Endianess != "big" and Endianess != "little":
                err = "\nError: Endianess must be either 'big' or 'little'"
                Usage(err)
                sys.exit(1)
            RunEndianBatch = 1
            BATCHMODE = 1
        if flag == "-m":
            BatchFile = arg
            RunHeaderBatch = 1
            BATCHMODE = 1
        if flag == "-t":
            BatchFile = arg
            RunTimeBatch = 1
            BATCHMODE = 1
        if flag == "-#":
            print(VERSION)
            sys.exit(0)
    if RunTimeBatch + RunHeaderBatch + RunEndianBatch > 1:
        err = ("\nERROR: Cannot correct timing, headers and/or endianess "
               "simultaniously\n")
        Usage(err)
        sys.exit(1)
print("\n", os.path.basename(sys.argv[0]), VERSION)


# main
########################################


class MainWindow(QWidget):
    def __init__(self, title='', BatchFile=''):
        super().__init__()

        # set timezone to UTC
        os.environ['TZ'] = 'GMT'
        time.tzname = ('GMT', 'GMT')
        time.timezone = 0

        # if a batch file exists on the command line,
        # we just run necessary processes for modifying headers
        self.BatchFile = BatchFile
        if BATCHMODE:
            self.runBatch()

        # else build GUI
        self.resize(650, 400)
        self.setWindowTitle(title)
        self.initGuiVars()
        self.buildRootWindow()
        self.setWidgetsVisible()  # hide widgets on start

########################################

    def initBatchVars(self):
        """
        Initialize variables needed for batchmode
        """
        if DATALOC:
            self.DataDirs = DATALOC
        else:
            self.DataDirs = os.getcwd()

        self.TimeShiftName = ""
        self.TSStartTime = ""
        self.TSEndTime = ""
        self.TSShift = 0.0
        self.TimeTagQuest = ""
        self.TypeTimeCor = 0
        self.ShiftVars = [
            ["Start_Time", self.TSStartTime],
            ["End_Time", self.TSEndTime],
            ["Time_Shift_sec", self.TSShift],
            ["Time_Tag_Quest", self.TimeTagQuest],
            ["Corr_Type", self.TypeTimeCor]
        ]
        self.UseNewOnlyTime = 0
        self.WildLoc = 0

########################################

    def runBatch(self):
        """
        If batchmode then launch from here and exit
        """
        # init necessary vars
        self.initBatchVars()

        # address endian change first since unique i.e. no batchfile
        print("Running in Batch Mode")
        if RunEndianBatch:
            print("Changing Endianess to: ", Endianess)
            self.ToEndian = Endianess
            self.RunChangeEndian = 1
            print("Finding Files beneath directory: ",
                  self.DataDirs)
            self.buildTrcList()
            self.changeEndian()
        else:
            if RunTimeBatch:
                print("Correcting Timing")
            if RunHeaderBatch:
                print("Correcting Headers")

            print("Using Template: ", self.BatchFile)
            self.loadTemplate()
            if RunTimeBatch:
                if not self.UpdateTimeDict:
                    print("No Timing Corrections in Template.\nDone")
                    sys.exit(1)
                print("Finding Files beneath directory: ",
                      self.DataDirs)
                self.buildTrcList()
                self.RunApplyTimeCor = 1
                self.applyTimeCor()
            if RunHeaderBatch:
                if not self.UpdateHdrDict:
                    print("No Header Corrections in Template.\nDone")
                    sys.exit(1)
                print("Finding Files beneath directory: ",
                      self.DataDirs)
                self.buildTrcList()
                self.RunModHdrs = 1
                self.modHdrs()
        print("Done")
        sys.exit(0)

########################################

    def initGuiVars(self):
        """
        Initialize variables, lists, and dictionaries
        associated with traces and trace headers for GUI
        """
        # Start Global Variables
        self.e_width = 24
        self.Start = 0

        # child top level window
        self.childTL = None

        # Variables, lists, dictionaries associated with traces and trace
        # headers
        self.OldSID = ""
        self.THDataDirs_le = QLineEdit()
        self.GMDataDirs_le = QLineEdit()
        self.TSDataDirs_le = QLineEdit()
        self.EDataDirs_le = QLineEdit()

        # data directory line edits to update
        # across all tabs
        self.DataDirLeList = [
            self.THDataDirs_le,
            self.GMDataDirs_le,
            self.TSDataDirs_le,
            self.EDataDirs_le
        ]

        self.StatSel_le = QLineEdit()
        self.StatSelList = []
        self.TraceDict = {}
        self.UpdateHdrDict = {}
        self.StatChanList = []
        self.StationName_cb = QComboBox()
        self.Station_le = QLineEdit()
        self.NetCode_le = QLineEdit()
        self.SampleRate_le = QLineEdit()
        self.Channel_le = QLineEdit()
        self.LocCode_le = QLineEdit()
        self.StatChanListVars = [
            ["Stat:Chan:Loc:Net:Sps", self.StationName_cb],
            ["Station_Name", self.Station_le],
            ["Channel", self.Channel_le],
            ["Location_Code", self.LocCode_le],
            ["Network_Code", self.NetCode_le],
            ["Sample_Rate", self.SampleRate_le]
        ]

        # New Values on Trace Headers tab
        self.NStation_le = QLineEdit()
        self.NChannel_le = QLineEdit()
        self.NLocCode_le = QLineEdit()
        self.NNetCode_le = QLineEdit()

        # Modified button on Trace Headers tab
        # denote whether applied updates or not
        self.SetHdrDict = {}
        self.SetStat = QCheckBox()
        self.SetChan = QCheckBox()
        self.SetLoc = QCheckBox()
        self.SetNet = QCheckBox()
        self.NewHdrVars = [
            ["Station_Name", self.NStation_le, self.SetStat],
            ["Channel", self.NChannel_le, self.SetChan],
            ["Location_Code", self.NLocCode_le, self.SetLoc],
            ["Network_Code", self.NNetCode_le, self.SetNet]
        ]

        # disable checkboxes so user cannot interact with them;
        # set style when update applied/not applied
        for val in self.NewHdrVars:
            val[2].setDisabled(True)
            val[2].setStyleSheet(
                "QCheckBox::indicator::unchecked{background-color:lightgray;}"
                "QCheckBox::indicator::checked{background-color:green;}"
                "QCheckBox{margin-left:12px;}")

        # vars used in global nb
        self.GlobalStation_cb = QComboBox()
        self.GlobalChannel_cb = QComboBox()
        self.GlobalLocCode_cb = QComboBox()
        self.GlobalSps_cb = QComboBox()
        self.GlobalNetCode_cb = QComboBox()

        # global update variables
        self.GUStation_le = QLineEdit()
        self.GUChannel_le = QLineEdit()
        self.GULocCode_le = QLineEdit()
        self.GUSps_le = QLineEdit()
        self.GUNetCode_le = QLineEdit()

        # lists for dropdown menus on Global Modify tab
        self.StationList = []
        self.ChannelList = []
        self.LocList = []
        self.NetList = []
        self.SpsList = []

        self.GlobalListVars = [
            ["Station_Name", self.GlobalStation_cb, self.StationList,
             self.GUStation_le],
            ["Channel", self.GlobalChannel_cb, self.ChannelList,
             self.GUChannel_le],
            ["Location_Code", self.GlobalLocCode_cb, self.LocList,
             self.GULocCode_le],
            ["Network_Code", self.GlobalNetCode_cb, self.NetList,
             self.GUNetCode_le],
            ["Sample_Rate", self.GlobalSps_cb, self.SpsList,
             self.GUSps_le]
        ]

        # vars used in TimeShift nb
        self.TimeShiftName_cb = QComboBox()
        self.OldShiftName = ""
        self.TSStartTime_le = QLineEdit()
        self.TSEndTime_le = QLineEdit()
        self.TSShift_le = QLineEdit()
        self.TypeTimeCor = 0
        self.AddToTimeCor_rb = QRadioButton("Add To")
        self.AddToTimeCor_rb.clicked.connect(
            lambda: self.setValue(self.TypeTimeCor, 0))
        self.AddToTimeCor_rb.setChecked(True)
        self.ReplaceTimeCor_rb = QRadioButton("Replace")
        self.ReplaceTimeCor_rb.clicked.connect(
            lambda: self.setValue(self.TypeTimeCor, 1))
        self.TimeTagQuest_cb = QComboBox()
        self.TimeTagQuest_cb.setCurrentText("")
        self.WildLoc = QCheckBox("Wildcard Location Code")
        self.UpdateTimeDict = {}
        self.TraceTimeDict = {}
        self.SetTimeDict = {}
        self.TimeCorrectedFiles = {}

        self.ShiftVars = [
            ["Start_Time", self.TSStartTime_le],
            ["End_Time", self.TSEndTime_le],
            ["Time_Shift_sec", self.TSShift_le],
            ["Time_Tag_Quest", self.TimeTagQuest_cb],
            ["Corr_Type", self.TypeTimeCor]
        ]

        self.SetTime = QCheckBox()
        self.SetTQuest = QCheckBox()
        self.SetTimeVars = [
            ["Time_Shift_sec", self.SetTime],
            ["Time_Tag_Quest", self.SetTQuest]
        ]
        for cb in self.SetTimeVars:
            cb[1].setDisabled(True)
            cb[1].setStyleSheet(
                "QCheckBox::indicator::unchecked{background-color:lightgray;}"
                "QCheckBox::indicator::checked{background-color:green;}")

        # vars used in Endianess nb
        self.Savefile = ""
        self.DataDirList = []
        self.BigEndianList = []
        self.LittleEndianList = []
        self.NumLittleFiles = 0
        self.NumBigFiles = 0
        self.ToEndian = ""

        # log vars and lists
        self.All = QRadioButton("All")
        self.All.setChecked(True)
        self.Header = QRadioButton("Header")
        self.Timing = QRadioButton("Timing")
        self.Endian = QRadioButton("Endian")
        self.AllText = QTextEdit()
        self.HeaderText = QTextEdit()
        self.TimingText = QTextEdit()
        self.EndianText = QTextEdit()
        self.LogList = [
            [self.All, self.AllText],
            [self.Header, self.HeaderText],
            [self.Timing, self.TimingText],
            [self.Endian, self.EndianText]
        ]

        for rb in self.LogList:
            rb[0].clicked.connect(self.displayLog)

        self.LogVar = 0
        self.LogAll = []
        self.LogHeader = []
        self.LogTime = []
        self.LogEndian = []
        self.LogSummary = []

        # activity and warning vars
        self.ActiveText = ""
        self.AlteredText = ""

        # set of vars watched for active threading event
        self.RunBuilddb = 0
        self.RunModHdrs = 0
        self.RunApplyTimeCor = 0
        self.RunChangeEndian = 0
        self.FoundFiles = 0
        self.RWErrors = 0
        self.CancelWarn = 0
        self.IgnoreWarn = 0
        self.UseNewOnlyTime = 0

        # widgets with tool tips
        self.ToolTipList = []

        # widgets to hide when
        # starting program
        self.HideOnStartList = []

        # if DATALOC not set on commandline set here
        if DATALOC:
            for le in self.DataDirLeList:
                le.setText(DATALOC)
        else:
            for le in self.DataDirLeList:
                le.setText(os.getcwd())

########################################

    def createTabs(self):
        """
        Set up and build tabs for root window
        """
        tabs = QTabWidget()
        tabs.addTab(QWidget(), "Trace Headers")
        self.buildTraceHeaders(tabs)

        tabs.addTab(QWidget(), "Global Modify")
        self.buildGlobalModify(tabs)

        tabs.addTab(QWidget(), "Time Shift")
        self.buildTimeShift(tabs)

        tabs.addTab(QWidget(), "Endianess")
        self.buildEndian(tabs)

        tabs.addTab(QWidget(), "Log")
        self.buildLog(tabs)

        tabs.addTab(QWidget(), "Help")
        self.buildHelp(tabs)

        tabs.addTab(QWidget(), "MSEED format")
        self.buildMSformat(tabs)

        # add tabs to root layout
        layout = QVBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(tabs)

########################################

    def buildRootWindow(self):
        """
        Build widgets for root window
        """
        # build tabs
        self.createTabs()

        # create info bar at bottom of root window
        self.InfoString = QLineEdit()
        self.InfoString.setAlignment(Qt.AlignCenter)
        self.InfoString.setStyleSheet(
            "QLineEdit{background-color: yellow;}")

        self.exit_b = QPushButton("Exit")
        self.exit_b.clicked.connect(self.exitCheck)
        self.exit_b.setToolTip("Exit Program")
        self.exit_b.setStyleSheet(
            "QPushButton::hover{background-color:red;}")

        self.save_b = QPushButton("Save Template")
        self.save_b.clicked.connect(lambda: self.saveWidget("templ"))
        self.save_b.setToolTip(
            "Export saved header & \ntiming corrections to file")
        self.setStyleSheet(
            "QPushButton::hover{background-color:green;}")

        self.load_b = QPushButton("Load Template")
        self.load_b.clicked.connect(self.loadTemplate)
        self.load_b.setToolTip(
            "Import saved header & \ntiming corrections file")
        self.load_b.setStyleSheet(
            "QPushBbtton::hover{background-color:green;}")

        self.PopUpHelp_cb = QCheckBox("PopUp Help")
        self.PopUpHelp_cb.setChecked(True)
        self.PopUpHelp_cb.clicked.connect(self.setToggleTips)
        self.PopUpHelp_cb.setToolTip(
            "Toggles 'PopUp Help' on and off")

        # add widgets to tooltip list
        self.ToolTipList.extend([
            self.exit_b, self.save_b, self.load_b, self.PopUpHelp_cb])

        # build groupbox and layout to hold root buttons
        box = QGroupBox()
        box_layout = QHBoxLayout(box)
        box_layout.setContentsMargins(0, 0, 0, 0)
        box_layout.addWidget(self.PopUpHelp_cb)
        box_layout.addStretch()
        box_layout.addWidget(self.load_b)
        box_layout.addWidget(self.save_b)
        box_layout.addWidget(self.exit_b)

        # add widgets to root layout
        self.layout().addWidget(box)
        self.layout().addWidget(self.InfoString)

########################################

    def setToggleTips(self):
        """
        Turns tooltips on and off
        """
        if self.PopUpHelp_cb.isChecked():
            for widget in self.ToolTipList:
                widget.setToolTipDuration(30000)
        else:
            for widget in self.ToolTipList:
                widget.setToolTipDuration(1)

########################################

    def buildTraceHeaders(self, tab):
        """
        Populate HdrList Tab
        tab for selecting traces, building trace database, and correcting
        headers one key at a time
        """
        # buttons
        ClearUpdateDict_b = QPushButton("Clear Update Dictionary")
        ClearUpdateDict_b.clicked.connect(
            lambda: self.clearUpdateDict(1))
        ClearUpdateDict_b.setToolTip(
            "Clears all entries \nin 'Update Dictionary'")
        ClearUpdateDict_b.setStyleSheet(
            "QPushButton::hover{background-color:orange;}")

        clear_b = QPushButton("Clear Current")
        clear_b.clicked.connect(self.clearTH)
        clear_b.setToolTip(
            "Clears entries in 'Update "
            "Dictionary' \nfor selected Stat:Chan:Loc:Net:Sps")
        self.setStyleSheet(
            "QPushButton::hover{background-color:orange;}")

        ModHdr_b = QPushButton("Modify Headers")
        ModHdr_b.clicked.connect(self.launchModHdrs)
        ModHdr_b.setToolTip(
            "Modifies headers using current "
            "entries \nand all entries in 'Update Dictionary'")
        ModHdr_b.setStyleSheet(
            "QPushButton{background-color:lightblue;}"
            "QPushButton::hover{background-color:green}")

        listtrace_b = QPushButton("List Traces")
        listtrace_b.clicked.connect(self.listTrace)
        listtrace_b.setToolTip(
            "Lists traces for selected \nStat:Chan:Loc:Net:Sps")
        listtrace_b.setStyleSheet(
            "QPushButton::hover{background-color:green;}")

        # data directory find and clear buttons, and entry field
        # button to build trace db
        DataDirs_l = QLabel("Data Directories:")
        DataDirs_l.setToolTip(
            "Search path(s) for finding mseed "
            "files. \nColon ""separate multiple entries")

        ClearDataDir_b = QPushButton("Clear")
        ClearDataDir_b.clicked.connect(
            lambda: self.setValue("DataDirs"))
        ClearDataDir_b.setToolTip(
            "Clears 'Data Directories' entry")

        FindDataDir_b = QPushButton("Find")
        FindDataDir_b.clicked.connect(self.getPath)
        FindDataDir_b.setToolTip(
            "Dialog window to \nselect Data Directories")
        FindDataDir_b.setStyleSheet(
            "QPushButton::hover{background-color:green;}"
        )

        BuildTrcList_b = QPushButton("Build Trace db")
        BuildTrcList_b.clicked.connect(self.buildTrcList)
        BuildTrcList_b.setToolTip(
            "Build a Trace db using \n'"
            "Data Directories' \nas top level directories")
        BuildTrcList_b.setStyleSheet(
            "QPushButton{background-color:lightblue;}"
            "QPushButton::hover{background-color:green;}")

        # station selection frame and entry box
        StatSel_l = QLabel("Find only stations "
                           "(colon separated list): ")
        self.StatSel_le.setToolTip(
            "Filter trace search \nfor these stations")

        ClearStatSel_b = QPushButton("Clear")
        ClearStatSel_b.clicked.connect(
            lambda: self.StatSel_le.clear())
        ClearStatSel_b.setToolTip("Clear station filter")
        ClearStatSel_b.setStyleSheet(
            "QPushButton::hover{background-color:orange;}")

        self.StationName_cb.currentIndexChanged.connect(self.fillTH)

        # add widgets to tooltip list
        self.ToolTipList.extend([
            ClearUpdateDict_b, clear_b, ModHdr_b,
            listtrace_b, DataDirs_l, ClearDataDir_b,
            FindDataDir_b, BuildTrcList_b, self.StatSel_le,
            ClearStatSel_b])

        # build groupboxes and layouts to hold widgets
        datadir_box = QGroupBox()
        datadir_layout = QHBoxLayout(datadir_box)
        datadir_layout.setContentsMargins(0, 0, 0, 0)
        datadir_layout.addWidget(DataDirs_l)
        datadir_layout.addWidget(self.THDataDirs_le)
        datadir_layout.addWidget(BuildTrcList_b)
        datadir_layout.addWidget(FindDataDir_b)
        datadir_layout.addWidget(ClearDataDir_b)

        statsel_box = QGroupBox()
        statsel_layout = QHBoxLayout(statsel_box)
        statsel_layout.setContentsMargins(0, 0, 0, 0)
        statsel_layout.addWidget(StatSel_l)
        statsel_layout.addWidget(self.StatSel_le)
        statsel_layout.addWidget(ClearStatSel_b)

        hdrs_box = QGroupBox()
        fnd_hdrs_layout = QFormLayout()
        space = QVBoxLayout()
        space.setContentsMargins(0, 0, 0, 0)
        new_hdrs_layout = QFormLayout()
        hdrs_layout = QHBoxLayout(hdrs_box)
        hdrs_layout.setContentsMargins(0, 0, 0, 0)
        hdrs_layout.addLayout(fnd_hdrs_layout)
        hdrs_layout.addLayout(space)
        hdrs_layout.addLayout(new_hdrs_layout)

        # build dropdown and entry boxes
        for var in self.StatChanListVars:
            fnd_hdrs_layout.addRow(QLabel(var[0]), var[1])

        spacer_item = QSpacerItem(
            0, 3, QSizePolicy.Fixed,
            QSizePolicy.Fixed)
        new_hdrs_layout.addItem(spacer_item)
        new_hdrs_layout.addRow(QLabel("New Values"), QLabel("Applied"))
        for var in self.NewHdrVars:
            new_hdrs_layout.addRow(var[1], var[2])

        # add space between two cols
        for _ in range(0, 6):
            space.addSpacerItem(
                QSpacerItem(
                    10, 0, QSizePolicy.Fixed,
                    QSizePolicy.Fixed))

        btn_box = QGroupBox()
        btn_box.setStyleSheet(
            "QGroupBox{border:0px;}")
        btn_layout = QHBoxLayout(btn_box)
        btn_layout.setContentsMargins(0, 0, 0, 0)
        btn_layout.addWidget(listtrace_b)
        btn_layout.addStretch()
        btn_layout.addWidget(ModHdr_b)
        btn_layout.addWidget(clear_b)
        btn_layout.addWidget(ClearUpdateDict_b)

        # add groupboxes to tab layout
        tab_layout = QVBoxLayout(tab.widget(0))
        tab_layout.setContentsMargins(0, 0, 0, 0)
        tab_layout.addWidget(datadir_box)
        tab_layout.addWidget(statsel_box)
        tab_layout.addWidget(hdrs_box)
        tab_layout.addStretch()
        tab_layout.addWidget(btn_box)

        # add widgets to hide list
        self.HideOnStartList.append(hdrs_box)

########################################

    def buildGlobalModify(self, tab):
        """
        Populate Global tab for making global changes to headers
        """
        # buttons
        ClearUpdateDict_b = QPushButton("Clear Update Dictionary")
        ClearUpdateDict_b.clicked.connect(
            lambda: self.clearUpdateDict(1))
        ClearUpdateDict_b.setToolTip(
            "Clears all entries \nin 'Update Dictionary'")
        ClearUpdateDict_b.setStyleSheet(
            "QPushButton::hover{background-color:orange;}")

        clear_b = QPushButton("Clear Current")
        clear_b.clicked.connect(self.clearGM)
        clear_b.setToolTip("Clears all \ncurrent entries")
        clear_b.setStyleSheet(
            "QPushButton::hover{background-color:orange;}")

        GlobalSet_b = QPushButton("Global Set")
        GlobalSet_b.clicked.connect(self.globalSet)
        GlobalSet_b.setToolTip(
            "Add current entries \nto 'Update Dictionary'")
        GlobalSet_b.setStyleSheet(
            "QPushButton{background-color:lightblue;}"
            "QPushButton::hover{background-color:green;}")

        # we let user see directory selection here but they don't have
        # access to manipulation buttons found on Trace Headers notebook
        DataDirs_l = QLabel("Current Data Directories:")
        DataDirs_l.setToolTip(
            "Traces located \nbeneath these \ndirectories")

        # add widgets to tooltip list
        self.ToolTipList.extend([
            ClearUpdateDict_b, clear_b, GlobalSet_b, DataDirs_l])

        # build groupboxes and layouts to hold widgets
        datadir_box = QGroupBox()
        datadir_layout = QHBoxLayout(datadir_box)
        datadir_layout.setContentsMargins(0, 0, 0, 0)
        datadir_layout.addWidget(DataDirs_l)
        datadir_layout.addWidget(self.GMDataDirs_le)

        grid = QGridLayout()
        grid.setContentsMargins(0, 0, 0, 0)
        mod_layout = QHBoxLayout()
        mod_layout.setContentsMargins(0, 0, 0, 0)
        mod_layout.addLayout(grid)
        mod_layout.addStretch()
        mod_box = QGroupBox()
        mod_box.setLayout(mod_layout)

        headers_l = QLabel("For Headers Matching:")
        subvals_l = QLabel("Substitute Values:")

        grid.addWidget(headers_l, 0, 2, alignment=Qt.AlignLeft)
        grid.addWidget(subvals_l, 0, 3, alignment=Qt.AlignLeft)
        r = 1
        c = 1
        for val in self.GlobalListVars:
            grid.addWidget(QLabel(val[0]), c, r, alignment=Qt.AlignLeft)
            r += 1
            widget = val[1]
            widget.setMinimumWidth(100)
            grid.addWidget(widget, c, r, alignment=Qt.AlignLeft)
            r += 1
            grid.addWidget(val[3], c, r, alignment=Qt.AlignLeft)
            c += 1
            r = 1
        clear_loc_b = QPushButton("Clear Loc Code")
        clear_loc_b.setToolTip(
            "Remove erroneous loc codes")
        self.ToolTipList.append(clear_loc_b)
        clear_loc_b.clicked.connect(lambda: self.GULocCode_le.setText("CLEAR"))
        grid.addWidget(clear_loc_b, 3, 4, alignment=Qt.AlignLeft)

        btn_box = QGroupBox()
        btn_box.setStyleSheet(
            "QGroupBox{border:0px;}")
        btn_layout = QHBoxLayout(btn_box)
        btn_layout.setContentsMargins(0, 0, 0, 0)
        btn_layout.addStretch()
        btn_layout.addWidget(GlobalSet_b)
        btn_layout.addWidget(clear_b)
        btn_layout.addWidget(ClearUpdateDict_b)

        # add groupboxes to tab layout
        tab_layout = QVBoxLayout(tab.widget(1))
        tab_layout.setContentsMargins(0, 0, 0, 0)
        tab_layout.addWidget(datadir_box)
        tab_layout.addWidget(mod_box)
        tab_layout.addStretch()
        tab_layout.addWidget(btn_box)

        # add widgets to hide list
        self.HideOnStartList.append(mod_box)

########################################

    def buildTimeShift(self, tab):
        """
        Populate Time Shift tab for applying time corrections to headers
        """
        # buttons
        clear_b = QPushButton("Clear All")
        clear_b.clicked.connect(self.resetTimeShift)
        clear_b.setToolTip("Clears all \ncurrent entries")
        clear_b.setStyleSheet(
            "QPushButton::hover{background-color:orange;}")

        ReCalc_b = QPushButton("Recalc Start/End")
        ReCalc_b.clicked.connect(self.recalcStartEnd)
        ReCalc_b.setToolTip(
            "Recalculate start and \nend times for selected traces")
        ReCalc_b.setStyleSheet(
            "QPushButton{background-color:lightblue;}"
            "QPushButton::hover{background-color:green;}")

        TimeSet_b = QPushButton("Time Set")
        TimeSet_b.clicked.connect(self.timeDictSet)
        TimeSet_b.setToolTip(
            "Add current entries \nto 'Update Dictionary'")
        TimeSet_b.setStyleSheet(
            "QPushButton{background-color:lightblue;}"
            "QPushButton::hover{background-color:green;}")

        ApplyTime_b = QPushButton("Apply Time Correction")
        ApplyTime_b.clicked.connect(self.launchApplyTimeCor)
        ApplyTime_b.setToolTip(
            "Modifies trace times using current entries "
            "\nand all entries in 'Update Dictionary'")
        ApplyTime_b.setStyleSheet(
            "QPushButton{background-color:lightblue;}"
            "QPushButton::hover{background-color:green;}")

        listtimecor_b = QPushButton("List Time Corrections")
        listtimecor_b.clicked.connect(self.listTimeCor)
        listtimecor_b.setToolTip(
            "List time corrections \ncurrently in trace "
            "headers \nfor select Stat:Loc:Net")
        listtimecor_b.setStyleSheet(
            "QPushButton::hover{background-color:green;}")

        # we let user see directory selection here but they don't have
        # access to manipulation buttons found on Trace Headers notebook
        DataDirs_l = QLabel("Current Data Directories:")
        DataDirs_l.setToolTip(
            "Traces located \nbeneath these \ndirectories")

        UndoTimeCor_b = QPushButton("Undo Time Corrections")
        UndoTimeCor_b.clicked.connect(self.launchUndoTimeCor)
        UndoTimeCor_b.setToolTip(
            "Reverse applied \ntime corrections")
        UndoTimeCor_b.setStyleSheet(
            "QPushButton{background-color:lightblue;}"
            "QPushButton::hover{background-color:green;}")

        TimeRad_l = QLabel("How to Treat Existing Corrections:")
        TimeRad_l.setToolTip(
            "When applying time corrections: \nAdd to "
            "existing or \nZero and replace existing")

        self.TimeShiftName_cb.currentIndexChanged.connect(
            self.loadTimeSelect)
        self.WildLoc.clicked.connect(self.wildCardLoc)

        # add widgets to tooltip list
        self.ToolTipList.extend([
            clear_b, ReCalc_b, TimeSet_b, ApplyTime_b,
            listtimecor_b, DataDirs_l, UndoTimeCor_b,
            TimeRad_l])

        # build groupboxes and layouts to hold widgets
        datadir_box = QGroupBox()
        datadir_layout = QHBoxLayout(datadir_box)
        datadir_layout.setContentsMargins(0, 0, 0, 0)
        datadir_layout.addWidget(DataDirs_l)
        datadir_layout.addWidget(self.TSDataDirs_le)

        rbtn_box = QGroupBox()
        rbtn_layout = QHBoxLayout(rbtn_box)
        rbtn_layout.setContentsMargins(0, 0, 0, 0)
        rbtn_layout.addWidget(TimeRad_l)
        rbtn_layout.addWidget(self.AddToTimeCor_rb)
        rbtn_layout.addWidget(self.ReplaceTimeCor_rb)
        rbtn_layout.addStretch()
        rbtn_layout.addWidget(UndoTimeCor_b)

        corr_box = QGroupBox()
        corr_layout = QHBoxLayout(corr_box)
        grid = QGridLayout()
        grid.setContentsMargins(0, 0, 0, 0)
        grid.addWidget(
            QLabel("Stat:Loc:Net"), 1, 0,
            alignment=Qt.AlignLeft)
        grid.addWidget(
            QLabel("For Traces Matching:"), 0, 1,
            alignment=Qt.AlignLeft)
        self.TimeShiftName_cb.setMinimumWidth(150)
        grid.addWidget(
            self.TimeShiftName_cb, 1, 1,
            alignment=Qt.AlignLeft)
        grid.addWidget(
            self.WildLoc, 2, 1,
            alignment=Qt.AlignLeft)
        grid.addWidget(
            QLabel(self.ShiftVars[0][0]), 1, 2,
            alignment=Qt.AlignRight)
        grid.addWidget(
            QLabel(self.ShiftVars[1][0]), 2, 2,
            alignment=Qt.AlignRight)
        grid.addWidget(
            QLabel(self.ShiftVars[2][0]), 3, 2,
            alignment=Qt.AlignRight)
        grid.addWidget(
            QLabel("Time Tag is questionable"), 4, 2,
            alignment=Qt.AlignRight)
        grid.addWidget(
            QLabel("Time Shift:"), 0, 3,
            alignment=Qt.AlignLeft)
        self.TSStartTime_le.setMinimumWidth(150)
        grid.addWidget(
            self.TSStartTime_le, 1, 3,
            alignment=Qt.AlignLeft)
        self.TSEndTime_le.setMinimumWidth(150)
        grid.addWidget(
            self.TSEndTime_le, 2, 3,
            alignment=Qt.AlignLeft)
        self.TSShift_le.setMinimumWidth(150)
        grid.addWidget(
            self.TSShift_le, 3, 3,
            alignment=Qt.AlignLeft)
        grid.addWidget(
            self.TimeTagQuest_cb, 4, 3,
            alignment=Qt.AlignLeft)
        grid.addWidget(
            QLabel("Applied"), 0, 4,
            alignment=Qt.AlignCenter)
        grid.addWidget(
            self.SetTime, 3, 4,
            alignment=Qt.AlignCenter)
        grid.addWidget(
            self.SetTQuest, 4, 4,
            alignment=Qt.AlignCenter)
        corr_layout.addLayout(grid)
        corr_layout.addStretch()

        btn_box = QGroupBox()
        btn_box.setStyleSheet(
            "QGroupBox{border:0px;}")
        btn_layout = QHBoxLayout(btn_box)
        btn_layout.setContentsMargins(0, 0, 0, 0)
        btn_layout.addWidget(listtimecor_b)
        btn_layout.addStretch()
        btn_layout.addWidget(ApplyTime_b)
        btn_layout.addWidget(TimeSet_b)
        btn_layout.addWidget(ReCalc_b)
        btn_layout.addWidget(clear_b)

        # add groupboxes to tab layout
        tab_layout = QVBoxLayout(tab.widget(2))
        tab_layout.setContentsMargins(0, 0, 0, 0)
        tab_layout.addWidget(datadir_box)
        tab_layout.addWidget(rbtn_box)
        tab_layout.addWidget(corr_box)
        tab_layout.addStretch()
        tab_layout.addWidget(btn_box)

        # add widgets to hide list
        self.HideOnStartList.append(corr_box)

########################################

    def buildEndian(self, tab):
        """
        Populate Endian tab for swapping endianess of headers
        """
        # buttons
        ToBig_b = QPushButton("Convert to Big")
        ToBig_b.clicked.connect(lambda: self.launchChangeEndian("big"))
        ToBig_b.setToolTip(
            "Convert all little \nendian files to \nbig endian")
        ToBig_b.setStyleSheet(
            "QPushButton{background-color:lightblue;}"
            "QPushButton::hover{background-color:green;}")

        ToLittle_b = QPushButton("Convert to Little")
        ToLittle_b.clicked.connect(lambda: self.launchChangeEndian("little"))
        ToLittle_b.setToolTip(
            "Convert all big \nendian files to \nlittle endian")
        ToLittle_b.setStyleSheet(
            "QPushButton{background-color:lightblue;}"
            "QPushButton::hover{background-color:green;}")

        little_l = QLabel("Little Endian Files Found:")
        big_l = QLabel("Big Endian Files Found:")

        self.NumLittle_le = QLineEdit()
        self.NumLittle_le.setAlignment(Qt.AlignRight)
        self.NumLittle_le.setText("0")
        self.NumLittle_le.setStyleSheet(
            "QLineEdit{background-color:yellow;}"
        )
        self.NumBig_le = QLineEdit()
        self.NumBig_le.setAlignment(Qt.AlignRight)
        self.NumBig_le.setText("0")
        self.NumBig_le.setStyleSheet(
            "QLineEdit{background-color:yellow;}"
        )

        # we let user see directory selection here but they don't have
        # access to manipulation buttons found on Trace Headers notebook
        DataDirs_l = QLabel("Current Data Directories:")
        DataDirs_l.setToolTip(
            "Traces located \nbeneath these \ndirectories")

        # add widgets to tooltip list
        self.ToolTipList.extend([
            ToBig_b, ToLittle_b, DataDirs_l])

        # build groupboxes and layouts to hold widgets
        datadir_box = QGroupBox()
        datadir_layout = QHBoxLayout(datadir_box)
        datadir_layout.setContentsMargins(0, 0, 0, 0)
        datadir_layout.addWidget(DataDirs_l)
        datadir_layout.addWidget(self.EDataDirs_le)

        endian_box = QGroupBox()
        endian_layout = QVBoxLayout(endian_box)
        endian_layout.setContentsMargins(0, 0, 0, 0)
        little_layout = QHBoxLayout()
        little_layout.addWidget(little_l)
        little_layout.addWidget(self.NumLittle_le)
        little_layout.addWidget(ToBig_b)
        little_layout.addStretch()
        big_layout = QHBoxLayout()
        big_layout.addWidget(big_l)
        big_layout.addWidget(self.NumBig_le)
        big_layout.addWidget(ToLittle_b)
        big_layout.addStretch()
        endian_layout.addLayout(little_layout)
        endian_layout.addLayout(big_layout)

        # add groupboxes to tab layout
        tab_layout = QVBoxLayout(tab.widget(3))
        tab_layout.setContentsMargins(0, 0, 0, 0)
        tab_layout.addWidget(datadir_box)
        tab_layout.addWidget(endian_box)
        tab_layout.addStretch()

########################################

    def buildLog(self, tab):
        """
        Populate Log tab for keeping track of changes
        """
        # buttons
        Clear_b = QPushButton("Clear Logs")
        Clear_b.clicked.connect(self.flushLogs)
        Clear_b.setStyleSheet(
            "QPushButton::hover{background-color:red;}")
        Clear_b.setToolTip("Delete all \nlog messages")

        Save_b = QPushButton("Save Log File")
        Save_b.clicked.connect(lambda: self.saveWidget("log"))
        Save_b.setToolTip("Save log \nmessages to file")
        Save_b.setStyleSheet(
            "QPushButton::hover{background-color:green;}")

        dump_b = QPushButton("Print Update Dict")
        dump_b.clicked.connect(lambda: self.dumpUpdateDicts("dump"))
        dump_b.setToolTip(
            "Print 'Update Dictionary' \nto log message window")
        dump_b.setStyleSheet(
            "QPushButton{background-color:lightblue;}"
            "QPushButton::hover{background-color:green;}")

        DisplayLog_l = QLabel("Display messages for:")
        DisplayLog_l.setToolTip("Filter log messages")

        self.LogStack = QStackedWidget()
        for val in self.LogList:
            self.LogStack.addWidget(val[1])

        # add widgets to tooltip list
        self.ToolTipList.extend([
            Clear_b, Save_b, DisplayLog_l,
            dump_b])

        # build groupboxes and layouts to hold widgets
        btn_box = QGroupBox()
        btn_layout = QHBoxLayout(btn_box)
        btn_layout.setContentsMargins(0, 0, 0, 0)
        btn_layout.addStretch()
        btn_layout.addWidget(dump_b)
        btn_layout.addWidget(Save_b)
        btn_layout.addWidget(Clear_b)

        rbtn_box = QGroupBox()
        rbtn_layout = QHBoxLayout(rbtn_box)
        rbtn_layout.setContentsMargins(0, 0, 0, 0)
        rbtn_layout.addWidget(DisplayLog_l)
        rbtn_layout.addWidget(self.All)
        rbtn_layout.addWidget(self.Header)
        rbtn_layout.addWidget(self.Timing)
        rbtn_layout.addWidget(self.Endian)
        rbtn_layout.addStretch()

        # add groupboxes to tab layout
        tab_layout = QVBoxLayout(tab.widget(4))
        tab_layout.setContentsMargins(0, 0, 0, 0)
        tab_layout.addWidget(btn_box)
        tab_layout.addWidget(rbtn_box)
        tab_layout.addWidget(self.LogStack)

########################################

    def buildHelp(self, tab):
        """
        Populate Help tab
        """
        TemplateFormat_b = QPushButton("Template Format")
        TemplateFormat_b.clicked.connect(self.templateFormat)
        TemplateFormat_b.setToolTip(
            "Display example format \nfor 'Template' file.")
        TemplateFormat_b.setStyleSheet(
            "QPushButton{background-color:lightblue;}"
            "QPushButton::hover{background-color:green;}")

        HelpText = QTextEdit()
        # colors for text
        Blue = QColor(0, 0, 153)
        Green = QColor(0, 100, 0)
        Red = QColor(136, 8, 8)
        Black = QColor(0, 0, 0)

        # fill text box
        # Name
        HelpText.setTextColor(Blue)
        HelpText.insertPlainText("NAME")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText("""
        fixhdr - a GUI to modify mseed file header fields
          ('Station Name', 'Channel', 'Location Code', and 'Network Code'),
          to apply time shifts and to convert header endianess.\n\n""")

        # Version
        HelpText.setTextColor(Blue)
        HelpText.insertPlainText("VERSION")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText("""
        %s\n\n""" % VERSION)

        # Synopsis
        HelpText.setTextColor(Blue)
        HelpText.insertPlainText("SYNOPSIS")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText("""
        fixhdr
        fixhdr -#
        fixhdr -h
        fixhdr [-d DataDirs] [-m batch_file] [-t batch_file] [-e endianess]
                                 \n""")

        # Options
        HelpText.setTextColor(Blue)
        HelpText.insertPlainText("OPTIONS")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText("""
        -# returns version number
        -h returns usage
        -d DataDirs colon separated list of data directories [default: cwd]
        -m batch_file correct mseed headers in batch mode using batch_file
        -t batch_file correct trace timing in batch mode using batch_file
        -e endianess convert trace headers to endianess [big or little]

        NOTE: -m, -t & -e are mutually exclusive. If you wish to do
        both timing and headers, run time corrections first.\n\n""")

        # Description
        HelpText.setTextColor(Blue)
        HelpText.insertPlainText("DESCRIPTION")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText("""
        fixhdr has seven notebooks: """)
        HelpText.setTextColor(Green)
        HelpText.insertPlainText(
            """[Trace Headers], [Global Modify], [Time Shift],
        [Endianess], [Log], [Help]""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText(" and ")
        HelpText.setTextColor(Green)
        HelpText.insertPlainText("[MSEED format]. [Trace Headers]")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText(" and ")
        HelpText.setTextColor(Green)
        HelpText.insertPlainText("[Global Modify]")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText("""
        provide a means to read and modify mseed header fields (station,""")
        HelpText.insertPlainText(" channel,")
        HelpText.insertPlainText("""
        location code, and network code) for files found beneath a specified
        directory list. """)
        HelpText.setTextColor(Green)
        HelpText.insertPlainText("[Time Shift] ")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText(
            """allows the user to apply a bulk time shift to
        traces that fall between a start and end time and to set a header
        flag indicating that the time tag is questionable. """)
        HelpText.setTextColor(Green)
        HelpText.insertPlainText("[Endianess] ")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText("""converts traces
        between little and big, or big and little endian headers. The """)
        HelpText.setTextColor(Green)
        HelpText.insertPlainText("[Log] ")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText("""notebook
        maintains records of key events.\n""")
        HelpText.setTextColor(Green)
        HelpText.insertPlainText("""
        [Root Window]""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText("""
        Buttons:""")
        HelpText.setTextColor(Red)
        HelpText.insertPlainText("""
            <Load Template>""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText(
            """: loads a previously saved or user created mapping
                of header and timing modification that then can be applied.""")
        HelpText.setTextColor(Red)
        HelpText.insertPlainText("""
            <Save Template>""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText(
            """: saves a map of all header and timing modifications.""")
        HelpText.setTextColor(Red)
        HelpText.insertPlainText("""
            <Exit>""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText(
            """: Exits fixhdr and will query if not all mappings in
                Update_Dictionary have been applied.""")
        HelpText.setTextColor(Green)
        HelpText.insertPlainText("""
        [Trace Headers]""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText("""
        General:
             >> specify/load directories for header investigation/modifications
             >> manipulate/edit individual <sta><chn><loc><net> entries for
                   loaded headers
             >> modify trace headers as specified in the "New Value" column
                   for _ALL_ loaded headers
             >> store and recall header modification templates
        Buttons:""")
        HelpText.setTextColor(Red)
        HelpText.insertPlainText("""
               <Build Trace db>""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText(
            """: Searchs the directories listed in the "Data-
            Directories" entry box (a colon separated list) and builds """)
        HelpText.insertPlainText("a list of mseed")
        HelpText.insertPlainText("""
            files found indexing them on unique values of""")
        HelpText.insertPlainText(
            "<sta><chn><loc><net><sps>.")
        HelpText.insertPlainText("""
            You can narrow your search by entering stations""")
        HelpText.insertPlainText(
            " in the \"Find Only Stations\"")
        HelpText.insertPlainText("""
            entry box (a colon separated list).""")
        HelpText.setTextColor(Red)
        HelpText.insertPlainText("""
               <Find>""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText(
            """: Launches a file browser allowing the user to
                    add directories to the "Data Directories" entry box.""")
        HelpText.insertPlainText(
            " Double clicking selects\n")
        HelpText.insertPlainText(
            "\tthe new directory.")
        HelpText.setTextColor(Red)
        HelpText.insertPlainText("""
               <Clear>""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText(
            """: Clears the "Data Directories" entry box.""")
        HelpText.setTextColor(Red)
        HelpText.insertPlainText("""
               <List Traces>""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText(
            """: Lists all traces for the selected Sta:Chan:Loc:Net:Sps.""")
        HelpText.setTextColor(Red)
        HelpText.insertPlainText("""
               <Modify Headers>""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText(
            """: Applies all current values in the Update_Dictionary
            \t(that can be viewed in the """)
        HelpText.setTextColor(Green)
        HelpText.insertPlainText("[Log]")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText(""" notebook) to the current trace list that
                    was built using """)
        HelpText.setTextColor(Red)
        HelpText.insertPlainText("<Build Trace db>.")
        HelpText.insertPlainText("""
               <Clear Current>""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText(
            """: clears all entries in the Update_Dictionary and
            \tdisplay for the currently selected sta:chan:loc:net:sps that
            \thave not been applied.""")
        HelpText.setTextColor(Red)
        HelpText.insertPlainText("""
               <Clear All>""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText(
            """: clears all entries in the Update_Dictionary that
            \thave not been applied.\n""")
        HelpText.setTextColor(Green)
        HelpText.insertPlainText("""
        [Global Modify]""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText("""
        General:
            >> specify header values in "For Headers Matching:" column,
                    using drop-down menus. Wildcards are allowed and
                    are the default.
            >> enter new header values in "Substitute Values:"
        Buttons:""")
        HelpText.setTextColor(Red)
        HelpText.insertPlainText("""
               <Global Set>""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText(
            """: inserts the values in "Substitute Values" into the "
            \tUpdate_Dictionary using For Headers Matching\"""")
        HelpText.insertPlainText(
            "entries to determine")
        HelpText.insertPlainText(
            "which\n\t<sta><chn><loc><net><sps> to modify.")
        HelpText.insertPlainText(
            " This only creates entries\n")
        HelpText.insertPlainText(
            "\tin the dictionary and does NOT apply them")
        HelpText.insertPlainText(
            "to the mseed headers.")
        HelpText.insertPlainText(" You must\n\tuse")
        HelpText.setTextColor(Green)
        HelpText.insertPlainText(" [Trace Headers]->")
        HelpText.setTextColor(Red)
        HelpText.insertPlainText("<Modify Headers> ")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText("to apply these updates.")
        HelpText.setTextColor(Red)
        HelpText.insertPlainText("""
               <Clear Loc Code>""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText(
            """: inserts the text \"CLEAR\" into the Substitue Value
                    for the selected Loc Code, indicating the selected Loc
                    Code will be cleared (emptied) after modifying.""")
        HelpText.setTextColor(Red)
        HelpText.insertPlainText("""
               <Clear All>""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText(""": clears all current entries. This does not
                    affect the Update_Dictionary.\n""")
        HelpText.setTextColor(Green)
        HelpText.insertPlainText("""
        [Time Shift]""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText("""
        General:
            >> specify header values in "For Traces Matching:" column, using
                    drop-down menus. Once selected first and last sample times
                    will be displayed. The start and end time can be changed.
            >> enter time in seconds to be added to the blockette start times
                    in the "Time_Shift_sec" window.
            >> "Time Tag is questionable" allows you to flip a bit in the Data
                    Quality flags in the mseed file's Fixed Section of Data
                    Header.
            >> "Applied" indicators notify a user when corrections have been
                    appied in this instance of fixhdr.
        Buttons:""")
        HelpText.setTextColor(Red)
        HelpText.insertPlainText("""
               <How to Treat Existing Corrections>""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText(""": you can choose to either add to any
                    correction listed in the Fixed Header or Replace
                    any existing correction with the one entered.""")
        HelpText.setTextColor(Red)
        HelpText.insertPlainText("""
               <Undo Time Corrections>""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText(""": Allows user to un-apply previously
                    applied timing corrections. Note: Use with caution. This
                    will only remove single and cumulative corrections.
                    Review 'List Time Corrections' to better understand
                    corrections already applied to trace headers.""")
        HelpText.setTextColor(Red)
        HelpText.insertPlainText("""
               <List Time Corrections>""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText(""": Displays time corrections for traces
                    matching the selected Stat:Loc:Net key.""")
        HelpText.setTextColor(Red)
        HelpText.insertPlainText("""
               <Apply Time Correction>""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText("to apply these updates.")
        HelpText.setTextColor(Red)
        HelpText.insertPlainText("""
               <Time Set>""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText(
            """: Allows user to build a dictionary of timing corrections
                to be applied with the "Apply Time Corrections" button. i.e.
                timing corrections for multiple Stat:Loc:Net selections can
                be set prior to applying in a single instance.""")
        HelpText.setTextColor(Red)
        HelpText.insertPlainText("""
               <Recalc Start/End>""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText(
            """: resets the start and end times from the trace
                headers. The first time a trace is read in the start and end
                times are cached for future use. If you change these times,
                this button is used to update the cache and display.""")
        HelpText.setTextColor(Red)
        HelpText.insertPlainText("""
               <Clear All>""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText(": clears all current entries.\n")
        HelpText.setTextColor(Green)
        HelpText.insertPlainText("""
        [Endianess]""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText("""
        General:
            >> displays the number of big and little endian files found.
                  Allows user to convert between big and little endian headers.
        Buttons:""")
        HelpText.setTextColor(Red)
        HelpText.insertPlainText("""
               <Convert to Big>""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText(
            ": Converts headers from little to big endian")
        HelpText.setTextColor(Red)
        HelpText.insertPlainText("""
               <Convert to Little>""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText(
            ": Converts headers from big to little endian\n")
        HelpText.setTextColor(Green)
        HelpText.insertPlainText("""
        [Log]""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText("""
        General:
            >> displays key events (e.g. "Build Trace db", "Modify Headers",
                etc.). Radio buttons allow you to select all log messages
                or window only those messages related to Header events, Time
                events, or changes in endianess
        Buttons:""")
        HelpText.setTextColor(Red)
        HelpText.insertPlainText("""
               <Dump UpdateHdrDict>""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText(
            """: Dumps the current values in the Update_Dictionary and
                indicates whether or not they have been applied (i.e. """)
        HelpText.setTextColor(Red)
        HelpText.insertPlainText("<Modify Headers>")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText("""
                    has been run).""")
        HelpText.setTextColor(Red)
        HelpText.insertPlainText("""
               <Save Log File>""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText(": Saves text window to an output file.")
        HelpText.setTextColor(Red)
        HelpText.insertPlainText("""
               <Clear Log File>""")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText(": Clears text window and flushes buffer.\n")

        # Keywords
        HelpText.setTextColor(Blue)
        HelpText.insertPlainText("\nKEYWORDS")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText("""
        mseed; header information; header modification\n""")

        # See also
        HelpText.setTextColor(Blue)
        HelpText.insertPlainText("\nSEE ALSO")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText("""
        mseedhdr & SEED manual\n""")
        HelpText.setTextColor(Blue)

        # Author
        HelpText.insertPlainText("\nAUTHOR")
        HelpText.setTextColor(Black)
        HelpText.insertPlainText("""
        Bruce Beaudoin <bruce@passcal.nmt.edu>
        """)

        # set cursor to beginning
        HelpText.moveCursor(QTextCursor.Start, QTextCursor.MoveAnchor)

        # add widgets to tooltip list
        self.ToolTipList.append(TemplateFormat_b)

        # build groupboxes and layouts to hold widgets
        btn_box = QGroupBox()
        btn_layout = QHBoxLayout(btn_box)
        btn_layout.setContentsMargins(0, 0, 0, 0)
        btn_layout.addStretch()
        btn_layout.addWidget(TemplateFormat_b)

        # add groupboxes to tab layout
        tab_layout = QVBoxLayout(tab.widget(5))
        tab_layout.setContentsMargins(0, 0, 0, 0)
        tab_layout.addWidget(btn_box)
        tab_layout.addWidget(HelpText)

########################################

    def buildMSformat(self, tab):
        """
        Populate MSEED format definition tab
        """

        MStext = QTextEdit()
        MStext.setText("""Appendix A From "SEED Reference Manual, v2.3, Feb. 1993:
Channel Naming
Contributed by Scott Halbert

Seismologists have used many conventions for naming channels.
Usually, these conventions are designed to meet the particular
needs of one network. But general recording systems - such as the
various Global Seismographic Network (GSN) systems that can
record many channels at high sample rates - create a need for a
standard to handle the variety of instruments that can be
recorded. Modern instrumentation and the need for conformity
among cooperating networks have greatly complicated the problem.
Sensors are available in narrow band and broadband configurations
with pass bands in very different parts of the spectrum of
interest. Each sensor may have several different outputs with
different spectral shaping. In addition, station processors often
derive several data streams from one sensor channel by digital
filtering. These possibilities require a comprehensive
convention. The desire to combine data from cooperating networks
and to search for like channels automatically requires
standardization. The SEED format uses three letters to name
seismic channels, and three letters to name weather or
environmental channels. In the following convention, each letter
describes one aspect of the instrumentation and its digitization.
SEED does not require this convention, but we recommend it as a
usage standard for Federation members to facilitate data
exchange.

Band Code
The first letter specifies the general sampling rate and the
response band of the instrument. (The "A" code is reserved for
administrative functions such as miscellaneous state of health.)
        Band code   Band type\t\tSample rate (Hz)   Corner period (sec)
        E\t        Extremely Short Period\t>= 80\t          < 10 sec
        S\t        Short Period\t\t>= 10 to < 80      < 10 sec
        H\t        High Broad Band\t\t>= 80\t          >= 10 sec
        B\t        Broad Band\t\t>= 10 to < 80      >= 10 sec
        M\t        Mid Period\t\t> 1 to < 10
        L\t        Long Period\t\t~ 1
        V\t        Very Long Period\t\t~ 0.1
        U\t        Ultra Long Period\t\t~ 0.01
        R\t        Extremely Long Period\t~ 0.001
        A\t        Administrative
        W\t        Weather/Environmental
        X\t        Experimental


Instrument Code and Orientation Code
The second letter specifies the family to which the sensor
belongs. The third letter specifies the physical configuration of
the members of a multiple axis instrument package or other
parameters as specified for each instrument.

Seismometer: Measures displacement/velocity/acceleration along a
line defined by the dip and azimuth.
        Instrument Code
        H\t\tHigh Gain Seismometer
        L\t\tLow Gain Seismometer
        G\t\tGravimeter
        M\t\tMass Position Seismometer
        N*\t\tAccelerometer
        *historically some channels from accelerometers have used
        instrumentation codes of L and G. The use of N is the FDSN
        convention as defined in August 2000.
        Orientation Code
        Z N E\t\tTraditional (Vertical, North-South, East-West)
        A B C\t\tTriaxial (Along the edges of a cube turned up on a corner)
        T R\t\tFor formed beams (Transverse, Radial)
        1 2 3\t\tOrthogonal components but non traditional orientations
        U V W\t\tOptional components
        Dip/Azimuth:\tGround motion vector (reverse dip/azimuth
                \t\tif signal polarity incorrect)
        Signal Units:\tM, M/S, M/S**2, (for G & M) M/S**2 (usually)
        Channel Flags:\tG

Tilt Meter: Measures tilt from the horizontal plane. Azimuth is
typically N/S or E/W.
        Instrument Code
        A
        Orientation Code
        N E\t\tTraditional
        Dip/Azimuth:\tGround motion vector (reverse dip/azimuth
                \t\tif signal polarity incorrect)
        Signal Units:\tRadians
        Channel Flags:\tG

Creep Meter: Measures the absolute movement between two sides of
a fault by means of fixing a metal beam on one side of the fault
and measuring its position on the other side. This is also done
with light beams.
The orientation and therefore the dip and azimuth would be
perpendicular to the measuring beam (light or metal), which would
be along the average travel vector for the fault.
Positive/Negative travel would be arbitrary, but would be noted
in the dip/azimuth. Another type of Creep Meter involves using a
wire that is stretched across the fault. Changes in wire length
are triangulated to form movement vector.
        Instrument Code
        B
        Orientation Code
        Unknown
        Dip/Azimuth:\tAlong the fault or wire vector
        Signal Units:\tM
        Channel Flags:\tG

Calibration Input: Usually only used for seismometers or other
magnetic coil instruments. This signal monitors the input signal
to the coil to be used in response evaluation. Usually tied to a
specific instrument. Sometimes all instruments are calibrated
together, sometimes horizontals are done separately from
verticals.
        Instrument Code
        C
        Orientation Code
        A B C D... for when there are only a few cal sources for many devices.
        Blank if there is only one calibrator at a time or, Match
        Calibrated Channel (i.s. Z, N or E)

Pressure: A barometer, or microbarometer measures pressure. Used
to measure the weather pressure or sometimes for state of health
monitoring down hole. This includes infrasonic and hydrophone
measurements.
        Instrument Code
        D
        Orientation Code
        O\t\tOutside
        I\t\tInside
        D\t\tDown Hole
        F\t\tInfrasound
        H\t\tHydrophone
        U\t\tUnderground
        Dip/Azimuth:\tNot applicable - Should be zero.
        Signal Units:\tPa (Pascals)
        Channel Flags:\tW or H

Electronic Test Point: Used to monitor circuitry inside recording
system, local power or seismometer. Usually for power supply
voltages, or line voltages.
        Instrument Code
        E
        Orientation code
        Designate as desired, make mnemonic as possible, use numbers
                \t\tfor test points, etc.
        Dip/Azimuth:       \tNot applicableSignal Units: V, A, Hz, Etc.
        Channel Flags:     \tH

Magnetometer: Measures the magnetic field where the instrument is
sitting. They measure the part of the field vector which is
aligned with the measurement coil. Many magnetometers are three
axis. The instrument will typically be oriented to local magnetic
north. The dip and azimuth should describe this in terms of the
geographic north. Example: Local magnetic north is 13 degrees
east of north in Albuquerque. So if the magnetometer is pointed
to magnetic north, the azimuth would be + 103 for the E channel.
Some magnetometers do not record any vector quantity associated
with the signal, but record the total intensity. So, these would
not have any dip/azimuth.
        Instrument Code
        F
        Orientation Code
        Z N E              \tMagnetic
        Signal Units:      \tT - Teslas
        Channel Flags:     \tG

Humidity: Absolute/Relative measurements of the humidity.
Temperature recordings may also be essential for meaningful
results.
        Instrument Code
        I
        Orientation Code
        O                  \tOutside Environment
        I                  \tInside Building
        D                  \tDown Hole
        1 2 3 4            \tCabinet Sources
        All other letters available for mnemonic source types.
        Dip/Azimuth:       \tNot applicable - Should be zero.
        Signal Units:      \t%
        Channel Flags:     \tW

Temperature: Measurement of the temperature at some location.
Typically used for measuring:
   1. Weather - Outside Temperature
   2. State of Health - Inside recording building
   - Down hole
   - Inside electronics
        Instrument Code
        K
        Orientation Code
        O                  \tOutside Environment
        I                  \tInside Building
        D                  \tDown Hole
        1 2 3 4            \tCabinet sources
        All other letters available for mnemonic types.
        Dip Azimuth:       \tNot applicable - Should be zero.
        Signal Units:      \tdeg C or deg K
        Channel Flags:     \tW or H

Water Current: This measurement measures the velocity of water in
a given direction. The measurement may be at depth, within a
borehole, or a variety of other locations.
        Instrument Code
        O
        Orientation Code
        Unknown
        Dip/Azimuth:       \tAlong current direction
        Signal Units:      \tM/S
        Channel Flags:     \tG

Geophone: Very short period seismometer, with natural frequency 5
- 10 Hz or higher.
        Instrument Code
        P
        Orientation Code
        Z N E              \tTraditional
        Dip/Azimuth:       \tGround Motion Vector (Reverse dip/azimuth
                \t\tif signal polarity incorrect)
        Signal Units:      \tM, M/S, M/S
        Channel Flags:     \tG

Electric Potential: Measures the Electric Potential between two
points. This is normally done using a high impedance voltmeter
connected to two electrodes driven into the ground. In the case
of magnetotelleuric work, this is one parameter that must be
measured.
        Instrument Code
        Q
        Orientation Code
        Unknown
        Signal Units:      \tV - Volts
        Channel Flags:     \tG

Rainfall: Measures total rainfall, or an amount per sampling
interval.
        Instrument Code
        R
        Orientation Code
        Unknown
        Dip/Azimuth:       \tNot applicable - Should be zero.
        Signal Units:      \tM, M/S
        Channel Flags:     \tW

Linear Strain: One typical application is to build a very
sensitive displacement measuring device, typically a long quartz
rod. One end is affixed to a wall. On the free end, a pylon from
the floor reaches up to the rod where something measures the
position of the pylon on the rod (like a large LVDT). There are
also some interferometry projects which measure distance with
lasers. Dip/Azimuth are the line of the movement being measured.
Positive values are obtained when stress/distance increases,
negative, when they decrease.
        Instrument Code
        S
        Orientation Code
        Z N E Vertical,    \tNorth-South, East-West
        Dip/Azimuth:       \tAlong axis of instrument
        Signal Units:      \tM/M
        Channel Flags:     \tG

Tide : Not to be confused with lunar tidal filters or gravimeter
output. Tide instruments measure the depth of the water at the
monitoring site.
        Instrument Code
        T
        Orientation Code
        Z                  \tAlways vertical
        Dip/Azimuth:       \tAlways vertical
        Signal Units:      \tM - Relative to sea level or local ocean depth
        Channel Flags:     \tG

Bolometer: Infrared instrument used to evaluate average cloud
cover. Used in astronomy to determine observability of sky.
        Instrument Code
        U
        Orientation Code
        Unknown
        Dip/Azimuth:       \tNot applicable - Should be zero.
        . Signal Units:    \tUnknown
        Channel Flags:     \tW

Volumetric Strain: Unknown
        Instrument Code
        V
        Orientation Code
        Unknown
        Dip/Azimuth:       \tNot Applicable - Should be zero.
        Signal Units:      \tM**3/M**3
        Channel Flags:     \tG

Wind: Measures the wind vector or velocity. Normal notion of dip
and azimuth does not apply.
        Instrument Code
        W
        Orientation Code
        S                  \tWind speed
        D                  \tWind Direction Vector - Relative to geographic North
        Dip/Azimuth:       \tNot Applicable - Should be zero.
        Channel Flags:     \tW

Synthesized Beams: This is used when forming beams from
individual elements of an array. Refer to blockettes 35, 400, &
405.
        Instrument Code
        Z
        Orientation Code
        I                  \tIncoherent Beam
        C                  \tCoherent Beam
        F                  \tFK Beam
        O                  \tOrigin Beam
        Dip/Azimuth:       \tGround motion vector (reverse dip/azimuth
                \t\tif signal polarity incorrect)
        Signal Units:      \tM, M/S, M/S**2, (for G & M) M/S**2 (usually)
        Channel Flags:     \tG

Channel Code
We suggest that two sequences be reserved for special channels
the "LOG" channel for the console log, and the "SOH" channel for
the main state of health channel. Subsidiary logs and state of
health channels should begin with the "A" code; the source and
orientation fields can then be used in any way.

Here are some typical channel arrangements used by a GSN system:
        Channel       \tDescription
        EHZ/EHN/EHE   \tShort Period 100 sps
        BHZ/BHN/BHE   \tBroad Band 20 sps
        LHZ/LHN/LHE   \tLong Period 1 sps
        VHZ/VHN/VHE   \tVery Long Period 0.1 sps
        BCI           \tBroad Band Calibration Signal
        ECI           \tShort Period Cal
        LOG           \tConsole Log

NOTE: Log Records: Log records has a channel identifier code of
"LOG" and a sample rate of zero. The number of samples field is
the number of characters in the record (including the carriage
return and line feed that terminates each line). Log messages are
packed into records until a message falls into a new minute. Log
records have no blockettes, so the strings start at offset
48.""")  # noqa: E501

        # add to tab layout
        tab_layout = QVBoxLayout(tab.widget(6))
        tab_layout.setContentsMargins(0, 0, 0, 0)
        tab_layout.addWidget(MStext)

########################################

    def buildLists(self):
        """
        Builds lists for drop downs in global modify and timeshift tabs
        """
        # clean any old entries
        del self.StationList[:]
        del self.ChannelList[:]
        del self.LocList[:]
        del self.NetList[:]
        del self.SpsList[:]

        # build lists for drop down selections
        for key in self.TraceDict:
            (stat, chan, loc, net, sps) = key.split(":")
            if stat not in self.StationList:
                self.StationList.append(stat)
            if not self.ChannelList.count(chan):
                self.ChannelList.append(chan)
            if not self.LocList.count(loc):
                self.LocList.append(loc)
            if not self.NetList.count(net):
                self.NetList.append(net)
            if not self.SpsList.count(sps):
                self.SpsList.append(sps)

        # add wildcard entry
        self.StationList.append("*")
        self.ChannelList.append("*")
        self.LocList.append("*")
        self.NetList.append("*")
        self.SpsList.append("*")

        # sort lists
        self.StationList = sorted(self.StationList)
        self.ChannelList = sorted(self.ChannelList)
        self.LocList = sorted(self.LocList)
        self.NetList = sorted(self.NetList)
        self.SpsList = sorted(self.SpsList)

########################################

    def buildTrcList(self):
        """
        Build a trace list using DataDirList as base directories
        """
        # initialize lists
        self.DataDirList = []
        self.StatSelList = []
        self.BigEndianList = []
        self.LittleEndianList = []
        self.TimeCorrectedFiles = {}
        n = 0

        self.addTextInfoBar()

        # wipe db
        if not BATCHMODE:
            self.clearAll()

        # split directory string & test they exist before launching into
        # self.FindTrace
        # populate DataDirList
        if not BATCHMODE:
            DataDirs = self.THDataDirs_le.text()
        else:
            DataDirs = self.DataDirs
        for idir in DataDirs.split(":"):
            dirlist = glob(idir)
            for newdir in dirlist:
                if not os.path.isdir(newdir):
                    err = "***WARNING*** Directory " + newdir + " not found."
                    self.addTextInfoBar(err, "red")
                    return
                self.DataDirList.append(newdir)

        # split station select list if it exists
        if not BATCHMODE:
            StatSel = self.StatSel_le.text()
            if StatSel == "*" or StatSel == "":
                pass
            else:
                for statsel in StatSel.split(":"):
                    statsel = statsel.strip()
                    self.StatSelList.append(statsel)

        self.RunBuilddb = 1

        # run findTrace as a thread
        if not BATCHMODE:
            self.initThread("Build Trace db",
                            self.launchfindTrace,
                            self.afterBuildTrace)
        else:
            self.launchfindTrace()

########################################

    def afterBuildTrace(self):
        """
        Update fields after findTrace finishes
        """
        # fill/update notebooks if not running batch mode
        if not BATCHMODE:
            info = "Building lists, updating frames"
            self.addTextInfoBar(info)
            self.updateHdrDropdown()
            self.buildLists()
            self.updateTimeShift()
            self.updateGlobalList()
            self.NumBigFiles = len(self.BigEndianList)
            self.NumBig_le.setText(str(self.NumBigFiles))
            self.NumLittleFiles = len(self.LittleEndianList)
            self.NumLittle_le.setText(str(self.NumLittleFiles))

            # if gui just launched
            # reveal hidden widgets
            if not self.Start:
                self.setWidgetsVisible("show")
                self.Start = 1

        # update user
        text = "\n" + str(self.FoundFiles) + \
            " unique mseed files found. *** " \
            + str(self.RWErrors) + " files with errors. See Log."
        self.addTextInfoBar(text, "green")
        QApplication.beep()

        if not BATCHMODE:
            # insert info on Log notebook
            self.writeLog("\n<Build Trace db>", "All", "blue")
            dirs = "Directories searched: " + self.THDataDirs_le.text()
            self.writeLog(dirs, "All", "black")
            self.writeLog(text, "All", "black")
            if self.StatChanList:
                self.writeLog("\t<Unique ID's found:>", "All", "green")
                for item in self.StatChanList:
                    self.writeLog("\t" + item, "All", "black")
                self.writeLog("\t<end>", "All", "green")
            self.writeLog("<end>", "All", "blue")

        # since we just rebuilt db no files have been altered
        self.AlteredText = ""

        if not self.odd_file_list:
            del self.odd_file_list
        else:
            self.genFixSizeTL()

    def launchfindTrace(self):
        """
        Separated out so FindTrace could be run as a thread
        """
        self.findTrace(self.DataDirList)

########################################

    def findTrace(self, DataDir):
        """
        Based on traverse routine in "python standard library", Lundh pg 34
        """
        # local variables
        stack = []
        losjoin = os.path.join
        losaccess = os.access
        loslistdir = os.listdir
        losisdir = os.path.isdir
        losislink = os.path.islink
        losisfile = os.path.isfile
        stackappend = stack.append
        stackpop = stack.pop
        appendBigEndian = self.BigEndianList.append
        appendLittleEndian = self.LittleEndianList.append
        # build stack of input directories
        for k in range(len(DataDir)):
            if not DataDir[k] in stack:
                stackappend(DataDir[k])
        # init several list and dictionaries
        files = []
        file_list = {}
        time_list = {}
        NumMseedFiles = 0
        rwError = 0
        cnt = 1
        self.odd_file_list = []

        # loop while directories still exist in stack
        while stack:
            directory = stackpop()
            if not losaccess(directory, 5):
                text = "Skipped: %s" % directory
                text1 = "\t\tAccess Error"
                self.writeLog(text, "All", "red")
                self.writeLog(text1, "All", "black")
                continue

            try:
                genfiles = (ifile for ifile in loslistdir(directory))
            except Exception as e:
                print("Directory Read Error: %s" % e)
                continue

            while True:
                try:
                    file = next(genfiles)
                except StopIteration:
                    break

                # respond to a cancel request
                if not self.RunBuilddb:
                    break
                # keep user posted of progress
                if mod(cnt, 10):
                    pass
                else:
                    if not BATCHMODE:
                        self.funcWorker.update.emit(cnt)
                    else:
                        self.wait("Examining File: ", cnt)
                # build up list of mseed files w/ full pathnames
                fullname = losjoin(directory, file)
                if losisfile(fullname):
                    if not losaccess(fullname, 6):
                        err = "ERROR: Read/Write permission denied. "\
                            "\n\t File:" + fullname
                        self.writeLog(err, "Header", "red")
                        rwError += 1
                        continue

                    try:
                        msfile = Mseed(fullname)
                        if msfile.isMseed():
                            try:
                                # simple test to determine if correct size file
                                filesize = msfile.filesize
                                blksize = msfile.blksize
                                (numblocks, odd_size) = divmod(
                                    filesize, blksize)
                                if odd_size:
                                    self.odd_file_list.append(fullname)
                                    continue

                            except Exception as e:
                                err = ("ERROR: Cannot determine file and "
                                       "block sizes. \n\t File:" + fullname)
                                self.writeLog(err, "Header", "red")
                                rwError += 1
                                continue
                            # assign header info
                            type = msfile.type
                            rate = msfile.rate
                            time = msfile.time
                            stat = msfile.FH.Stat.decode().strip()
                            chan = msfile.FH.Chan.decode().strip()
                            loc = msfile.FH.Loc.decode().strip()
                            net = msfile.FH.Net.decode().strip()

                            if not BATCHMODE:
                                # window on specific stations if exist
                                if self.StatSelList:
                                    if stat not in self.StatSelList:
                                        continue

                            # build endian lists
                            if msfile.byteorder == "big":
                                if fullname not in self.BigEndianList:
                                    appendBigEndian(fullname)
                            else:
                                if fullname not in self.LittleEndianList:
                                    appendLittleEndian(fullname)

                            # build file list for fixing headers
                            fileid = ":".join(map(str, (
                                stat, chan, loc, net, rate)))
                            # fileid= (str(stat) + ":" + chan + ":" + loc + ":"
                            #          + net + ":" + str(rate))
                            try:
                                if fullname not in file_list[fileid]:
                                    file_list[fileid].append(fullname)
                                    NumMseedFiles += 1
                            except KeyError:
                                file_list[fileid] = []
                                file_list[fileid].append(fullname)
                                NumMseedFiles += 1

                            # build empty time_list to fill later with
                            # GetStartEnd
                            timeid = ":".join(
                                list(map(str, (stat, loc, net))),)
                            # timeid = ":".join(map(str,(stat,loc,net)),)
                            # timeid=str(stat) + ":" + loc + ":" + net
                            # if not time_list.has_key(timeid) :
                            time_list[timeid] = []

                        msfile.close()
                    except Exception as e:
                        print("File Open Error: %s" % e)
                        print(fullname)
                cnt += 1

                # add fullname to stack if it is a directory or link
                if losisdir(fullname) or (losislink(fullname) and
                                          not losisfile(fullname)):
                    if fullname not in stack:
                        stackappend(fullname)

        self.TraceDict = file_list
        self.TraceTimeDict = time_list
        self.ActiveText = ""
        self.FoundFiles = NumMseedFiles
        self.RWErrors = rwError

########################################

    def genFixSizeTL(self):
        """
        Create top level window to ask user
        if they want to fix mseeds with variable
        record length
        """
        self.childTL = self.TopLevel("odd_file", self)
        self.childTL.show()

########################################

    def updateHdrDropdown(self):
        """
        Populate HdrList dropdown
        This is extracted from the buildHdrList to allow for
        dynamic update of header list
        """
        # build list for stat:chan:loc:net:sps selection dropdown
        self.StatChanList = sorted(list(self.TraceDict.keys()))
        self.clearTH()
        if not self.StatChanList:
            self.addTextInfoBar("No Data Found", 'orange')

        # disable signal so widget doesn't call slot
        self.StationName_cb.blockSignals(True)
        self.StationName_cb.addItems(self.StatChanList)
        self.StationName_cb.insertItem(0, "")
        self.StationName_cb.setCurrentIndex(0)
        self.StationName_cb.blockSignals(False)

########################################

    def fillTH(self):
        """
        Enters current header values in appropriate fields
        """
        # populate current header listing with new selection
        name = self.StationName_cb.currentText()

        self.clearTH()

        if not name:
            return

        self.StationName_cb.blockSignals(True)
        self.StationName_cb.setCurrentText(name)
        self.StationName_cb.blockSignals(False)
        val = name.split(":")
        self.Station_le.setText(val[0])
        self.Channel_le.setText(val[1])
        self.LocCode_le.setText(val[2])
        self.NetCode_le.setText(val[3])
        self.SampleRate_le.setText(val[4])

        # populate new values with UpdateHdrDict if exists for new selection
        if name in self.UpdateHdrDict:
            for var in self.NewHdrVars:
                if var[0] in self.UpdateHdrDict[name]:
                    setval = self.UpdateHdrDict[name][var[0]]
                    var[1].setText(setval)

        # fill in Applied buttons
        if name in self.SetHdrDict:
            for var in self.NewHdrVars:
                if var[0] in self.SetHdrDict[name]:
                    setval = self.SetHdrDict[name][var[0]]
                    if setval == 1:
                        var[2].setChecked(True)
        self.OldSID = name

        # give user info on traces selected
        cnt = len(self.TraceDict[name])
        textstr = str(cnt) + " traces selected for key: " + name
        self.addTextInfoBar(textstr, 'lightblue')

########################################

    def updateHdr(self, name=""):
        """
        Builds UpdateHdrDict based on entries in NewValues
        """
        # assign new values to UpdateHdrDict
        for var in self.NewHdrVars:
            testkey = self.OldSID
            if testkey in self.UpdateHdrDict:
                if var[1].text():
                    value = var[1].text().upper()
                    if value == "CLEAR":
                        value = " "
                    if var[0] in self.UpdateHdrDict[testkey]:
                        if self.UpdateHdrDict[testkey][var[0]] != value:
                            if testkey in self.SetHdrDict:
                                if var[0] in self.SetHdrDict[testkey]:
                                    del self.SetHdrDict[testkey][var[0]]
                    self.UpdateHdrDict[testkey][var[0]] = value
            else:
                if var[1].text():
                    self.UpdateHdrDict[testkey] = {}
                    value = var[1].text().upper()
                    if value == "CLEAR":
                        value = " "
                    self.UpdateHdrDict[testkey][var[0]] = value
        self.clearTH()

########################################

    def launchModHdrs(self):
        """
        Separated out so ModHdrs could be run as a thread
        """
        # if headers have already been alterred warn user
        if self.SetHdrDict or self.SetTimeDict:
            cancel = self.showWarnTL("headers")
            if cancel:
                return

        # if the user ignores warning proceed
        self.RunModHdrs = 1
        self.IgnoreWarn = 0

        # run as thread
        self.initThread("Modify Headers", self.modHdrs)

########################################

    def modHdrs(self):
        """
        Uses values in UpdateHdrDict to modify headers for traces in
        TraceDict
        """
        if not BATCHMODE:
            # self.showCancelTL(self.RunModHdrs, "Modify Headers")
            self.addTextInfoBar()

            # first make sure any current 'new values' are saved to
            # UpdateHdrDict
            statName = self.StationName_cb.currentText()
            self.updateHdr(statName)
            if not statName and not self.UpdateHdrDict:
                self.addTextInfoBar("No Station Selected", 'orange')
                return
            elif not self.UpdateHdrDict:
                self.addTextInfoBar("No Updates Specified", 'orange')
                return

        self.ActiveText = "Modify Headers Running"
        self.AlteredText = ("Headers or blockette times have been modified."
                            "\nRebuild database to avoid unexpected "
                            "results.\n")

        # now make mods to headers for all keys (sta:chan:loc:net:sps) in
        # UpdateHdrDict
        cnt = 0
        error_cnt = 0
        warn_cnt = 0
        inlist = sorted(list(self.UpdateHdrDict.keys()))
        for key in inlist:
            if key not in self.SetHdrDict:
                self.SetHdrDict[key] = {}
            if key in self.TraceDict:
                # set new values if exist and keep track
                # of modifications in SetHdrDict
                Stat = Loc = Chan = Net = ""
                skeylist = (skey for skey in self.UpdateHdrDict[key])
                while True:
                    try:
                        subkey = next(skeylist)
                    except StopIteration:
                        break
                    # for subkey in self.UpdateHdrDict[key].keys() :
                    value = self.UpdateHdrDict[key][subkey]
                    if subkey == "Station_Name":
                        Stat = value
                        self.SetHdrDict[key][subkey] = 1
                    if subkey == "Channel":
                        Chan = value
                        self.SetHdrDict[key][subkey] = 1
                    if subkey == "Location_Code":
                        Loc = value
                        self.SetHdrDict[key][subkey] = 1
                    if subkey == "Network_Code":
                        Net = value
                        self.SetHdrDict[key][subkey] = 1
                ifilelist = (ifile for ifile in self.TraceDict[key])
                # for infile in self.TraceDict[key] :
                # respond to cancel requests
                while True:
                    try:
                        infile = next(ifilelist)
                    except StopIteration:
                        break
                    if not self.RunModHdrs:
                        break
                    cnt += 1
                    if mod(cnt, 10):
                        pass
                    else:
                        if not BATCHMODE:
                            self.funcWorker.update.emit(cnt)
                        else:
                            self.wait("Modifying Trace: ", cnt)

                    # determine file size and get block size from
                    # Blockette 1000
                    msfile = Mseed(infile)
                    # filesize = msfile.filesize
                    blksize = msfile.blksize
                    numblocks = msfile.numblocks
                    # (numblocks, odd_size) = divmod(filesize, blksize)
                    # should not be needed, but not expensive
                    # warn="WARNING: File size is not an integer number "\
                    # "of block size (+ str(blksize) +"). File: " + infile
                    # self.writeLog(warn,"Header","red")
                    # warn_cnt+=1

                    # loop over all blocks and correct headers
                    blk = 0
                    while blk < numblocks:
                        hdrs = msfile.fixedhdr(blk * blksize)
                        (SeqNum, DHQual, res, oldStat, oldLoc, oldChan,
                         oldNet) = hdrs[0]
                        if not Stat:
                            Stat = oldStat
                        if not Loc:
                            Loc = oldLoc
                        if not Loc:
                            Loc = oldLoc
                        if not Chan:
                            Chan = oldChan
                        if not Net:
                            Net = oldNet
                        newhdrs = ((SeqNum, DHQual, res, Stat, Loc, Chan,
                                    Net), hdrs[1], hdrs[2], hdrs[3])
                        bytes_written = msfile.WriteFixedHdr(
                            newhdrs, blk * blksize)
                        blk += 1
                    msfile.close()

        if not BATCHMODE:
            # do some cleanup
            self.clearTH()

        if error_cnt or warn_cnt:
            text = SPACE.join(("Modified Headers in ", str(cnt - error_cnt),
                               " files. Encountered ", str(error_cnt),
                               " error(s) & ", str(warn_cnt), " warning(s)"))
            self.addTextInfoBar(text, "orange")
        else:
            text = SPACE.join(("Modified Headers in ", str(cnt), " files"))
            self.addTextInfoBar(text, "green")

        # insert info on Log notebook
        self.writeLog("\n<Modify Headers>", "Header", "blue")
        if error_cnt or warn_cnt:
            self.writeLog(text, "Header", "red")
        else:
            self.writeLog(text, "Header", "black")
        self.dumpUpdateDicts("mod")
        self.writeLog("<end>", "Header", "blue")

        self.ActiveText = ""

########################################

    def updateGlobalList(self):
        """
        Populate drop downs for global modify notebook
        """
        # clear drop downs except for "*"
        self.clearGM()

        self.GlobalListVars[0][1].addItems(self.StationList)
        self.GlobalListVars[1][1].addItems(self.ChannelList)
        self.GlobalListVars[2][1].addItems(reversed(self.LocList))
        self.GlobalListVars[3][1].addItems(self.NetList)
        self.GlobalListVars[4][1].addItems(self.SpsList)

########################################

    def globalSet(self):
        """
        Build UpdateHdrDict
        """
        self.addTextInfoBar()
        # check if mods are enterred
        if not self.GUStation_le \
           and not self.GUChannel_le \
           and not self.GULocCode_le \
           and not self.GUNetCode_le:
            self.addTextInfoBar("No 'Substitute Values' Given", 'orange')
            return

        for key in self.TraceDict:
            # make sure there are no lingering NewVars
            self.clearTH()
            (stat, chan, loc, net, sps) = key.split(":")
            if (self.GlobalStation_cb.currentText() == "*" or
                self.GlobalStation_cb.currentText() == stat) and \
               (self.GlobalChannel_cb.currentText() == "*" or
                self.GlobalChannel_cb.currentText() == chan) and \
               (self.GlobalLocCode_cb.currentText() == "*" or
                self.GlobalLocCode_cb.currentText() == loc) and \
               (self.GlobalNetCode_cb.currentText() == "*" or
                self.GlobalNetCode_cb.currentText() == net) and \
               (self.GlobalSps_cb.currentText() == "*" or
                    self.GlobalSps_cb.currentText() == sps):

                if self.GUStation_le.text():
                    self.NStation_le.setText(self.GUStation_le.text())
                    if key in self.SetHdrDict:
                        if "Station_Name" in self.SetHdrDict[key]:
                            del self.SetHdrDict[key]["Station_Name"]

                if self.GUChannel_le.text():
                    self.NChannel_le.setText(self.GUChannel_le.text())
                    if key in self.SetHdrDict:
                        if "Channel" in self.SetHdrDict[key]:
                            del self.SetHdrDict[key]["Channel"]
                if self.GULocCode_le.text():
                    self.NLocCode_le.setText(self.GULocCode_le.text())
                    if key in self.SetHdrDict:
                        if "Location_Code" in self.SetHdrDict[key]:
                            del self.SetHdrDict[key]["Location_Code"]
                if self.GUNetCode_le.text():
                    self.NNetCode_le.setText(self.GUNetCode_le.text())
                    if key in self.SetHdrDict:
                        if "Network_Code" in self.SetHdrDict[key]:
                            del self.SetHdrDict[key]["Network_Code"]
                self.OldSID = key
                self.updateHdr()

        # empty all entries
        self.clearTH()
        self.clearGM()
        info = "Global Set Done. Use 'Trace Headers->Modify Headers' "\
            "to apply."
        self.addTextInfoBar(info, 'green')

########################################

    def timeDictSet(self):
        """
        Build UpdateTimeDict
        """
        self.addTextInfoBar()
        # check input formats before committing to UpdateTimeDict
        if not BATCHMODE:
            TimeShiftName = self.TimeShiftName_cb.currentText()
        else:
            TimeShiftName = self.TimeShiftName
        if not TimeShiftName:
            if self.UpdateTimeDict:
                return 1
            else:
                self.addTextInfoBar("No 'Stat:Loc:Net' selected", 'red')
                return 0
        (startEpoch, err) = self.buildEpoch(self.TSStartTime_le.text())
        if err:
            err = err + ": Start_Time"
            self.addTextInfoBar("Invalid StartTime String", 'red')
            return 0

        (endEpoch, err) = self.buildEpoch(self.TSEndTime_le.text())
        if err:
            err = err + ": End_Time"
            self.addTextInfoBar("Invalid EndTime String", 'red')
            return 0

        try:
            self.TSShift_le.text()
            if self.TSShift_le.text() == "":
                err = "ERROR: Time_Shift_sec must be a float"
                self.addTextInfoBar(err, 'red')
                return 0
        except Exception as e:
            err = "ERROR: Time_Shift_sec must be a float"
            self.addTextInfoBar(err, 'red')
            return 0
        if self.TimeTagQuest_cb.currentText():
            if (self.TimeTagQuest_cb.currentText() != "set" and
               self.TimeTagQuest_cb.currentText() != "unset"):
                err = "ERROR: 'Time Tag is questionable' must be either "\
                    "'set', 'unset', or blank"
                self.addTextInfoBar(err, 'red')
                return 0

        # Create UpdateTimeDict entry. THIS OVERWRITES any previous entry
        # I overwrite since when a user selects Stat:Loc:Net on Notebook
        # they are informed if an entry exists and if it has been applied.
        # User beware
        (stat, loc, net) = self.TimeShiftName_cb.currentText().split(':')
        for key in self.TraceTimeDict:
            (TTD_stat, TTD_loc, TTD_net) = key.split(':')
            if TTD_stat == stat \
               and \
               (TTD_loc == loc or loc == "*") \
               and \
               TTD_net == net:
                self.UpdateTimeDict[key] = {}
                for var in self.ShiftVars:
                    if isinstance(var[1], QLineEdit):
                        self.UpdateTimeDict[key][var[0]] = var[1].text()
                    elif isinstance(var[1], QComboBox):
                        self.UpdateTimeDict[key][var[0]] = var[1].currentText()
                    else:
                        # int
                        self.UpdateTimeDict[key][var[0]] = var[1]
        # empty all entries
        # self.clearAll(self.StatChanListVars)
        info = "Time Set Done. Use 'Apply Time Corrections' to apply."
        self.addTextInfoBar(info, 'green')
        return 1

########################################

    def updateTimeShift(self):
        """
        Populates dropdowns for TimeShift tab
        """
        # build list for stat:loc:net selection dropdown
        self.StatLocNetList = sorted(list(self.TraceTimeDict.keys()))
        if not self.StatLocNetList:
            self.addTextInfoBar("No Data Found", 'orange')
        else:
            self.TimeShiftName_cb.blockSignals(True)
            self.TimeShiftName_cb.clear()
            self.TimeTagQuest_cb.clear()

            self.TimeShiftName_cb.addItems(self.StatLocNetList)
            self.TimeShiftName_cb.insertItem(0, "")
            self.TimeShiftName_cb.blockSignals(False)
            self.TimeTagQuest_cb.addItems(["", "set", "unset"])

        self.resetTimeShift()

########################################

    def wildCardLoc(self):
        """
        Sub loc code with *
        """
        if self.WildLoc.isChecked():
            if self.TimeShiftName_cb.currentText():
                self.OldShiftName = self.TimeShiftName_cb.currentText()
                newname = self.TimeShiftName_cb.currentText().split(":")
                newname[1] = "*"
                self.TimeShiftName_cb.setItemText(
                    self.TimeShiftName_cb.currentIndex(),
                    ":".join(newname))
            else:
                return
        else:
            if self.TimeShiftName_cb.currentText():
                self.TimeShiftName_cb.setItemText(
                    self.TimeShiftName_cb.currentIndex(),
                    self.OldShiftName)
            else:
                return

########################################

    def launchApplyTimeCor(self, undo=0):
        """
        Separated out so ApplyTimeCor could be run as a thread
        """
        # if headers have already been alterred warn user
        if not undo:
            if self.SetTimeDict or self.SetHdrDict:
                cancel = self.showWarnTL("time")
                if cancel:
                    return

        # if the user ignores warning proceed
        # set var indicating ApplyTimeCor is running. Watched to exit thread if
        # cancel button selected in CancelTL
        self.RunApplyTimeCor = 1
        # write log and user information
        self.writeLog("\n<Modify Time>", "Time", "blue")

        # run as thread
        self.initThread("Apply Time Corrections", self.applyTimeCor)

########################################

    def newOnlyTime(self):
        """
        Set flag to only use new (unapplied) timing corrections
        """
        self.ActiveText = ""
        self.AlteredText = ""
        self.UseNewOnlyTime = 1

########################################

    def applyTimeCor(self):
        """
        Apply timing correction to start time etc.
        """
        # Launch CancelTL allowing user to cancel process
        # if not BATCHMODE:
        #    self.showCancelTL(self.RunApplyTimeCor, "Apply Time Correction")

        if not self.timeDictSet():
            return

        num_cnt = 0
        err_cnt = 0
        skip_cnt = 0

        UpdateTimeDictKeys = list(self.UpdateTimeDict.keys())
        for key in UpdateTimeDictKeys:
            if key not in self.TraceTimeDict:
                continue
            # capture cancel
            if not self.RunApplyTimeCor:
                infotext = "Canceled Applied Corrections to: " + str(num_cnt)\
                    + " files."
                self.addTextInfoBar(infotext, 'orange')
                logtext = "\t" + infotext
                self.writeLog(logtext, "Time", "red")
                break
            if not BATCHMODE:
                self.TimeShiftName_cb.setCurrentText(key)
            else:
                self.TimeShiftName = key
            self.getStartEnd()
            for var in self.ShiftVars:
                if var[0] in self.UpdateTimeDict[key]:
                    if not BATCHMODE:
                        if isinstance(var[1], QLineEdit):
                            var[1].setText(
                                str(self.UpdateTimeDict[key][var[0]]))
                        elif isinstance(var[1], QComboBox):
                            var[1].setCurrentText(
                                self.UpdateTimeDict[key][var[0]])
                        else:
                            # int
                            var[1] = (self.UpdateTimeDict[key][var[0]])
                    else:
                        var[1] = self.UpdateTimeDict[key][var[0]]

            self.addTextInfoBar()

            self.ActiveText = "Time Shift Running"
            self.AlteredText = ("Headers or blockette times have been "
                                "modified.\n'Apply Unapplied Only\' to "
                                "avoid unexpected results.\n")

            # build epoch start & end time for correction
            if not BATCHMODE:
                TSStartTime = self.TSStartTime_le.text()
            else:
                TSStartTime = self.TSStartTime
            (startEpoch, err) = self.buildEpoch(TSStartTime)
            if err:
                err = err + ": Start_Time"
                self.addTextInfoBar("Invalid StartTime String", 'red')
                return

            if not BATCHMODE:
                TSEndTime = self.TSEndTime_le.text()
            else:
                TSEndTime = self.TSEndTime
            (endEpoch, err) = self.buildEpoch(TSEndTime)
            if err:
                err = err + ": End_Time"
                self.addTextInfoBar("Invalid EndTime String", 'red')
                return

            # write log and user information
            # self.writeLog("\n<Modify Time>\n", "Time", "blue")
            logtext = "\n\tKey Name: " + key
            self.writeLog(logtext, "Time", "black")
            cnt = len(self.TraceTimeDict[key]) - 1  # first record
            logtext = "\tNumber of Traces Selected: " + str(cnt)
            self.writeLog(logtext, "Time", "black")
            if key in self.TimeCorrectedFiles:
                changedfiles = len(self.TimeCorrectedFiles[key])
                if self.UseNewOnlyTime:
                    logtext = "\tSkipping " + str(changedfiles) + \
                        " previously corrected files."
                    self.writeLog(logtext, "Time", "black")
                else:
                    logtext = "\tApplying Corrections to " + str(changedfiles)\
                        + " previously corrected files."
                    self.writeLog(logtext, "Time", "red")

            if not BATCHMODE:
                TSStartTime = self.TSStartTime_le.text()
            else:
                TSStartTime = self.TSStartTime
            logtext = "\tStart_Time of Correction: " + \
                str(TSStartTime)

            if not BATCHMODE:
                TSEndTime = self.TSEndTime_le.text()
            else:
                TSEndTime = self.TSEndTime
            self.writeLog(logtext, "Time", "black")
            logtext = "\tEnd_Time of Correction: " + \
                str(TSEndTime)
            self.writeLog(logtext, "Time", "black")

            if not BATCHMODE:
                TSShift = self.TSShift_le.text()
            else:
                TSShift = self.TSShift
            logtext = "\tTime_Shift_sec " + str(TSShift)
            self.writeLog(logtext, "Time", "black")

            if self.TypeTimeCor:
                type = "Replace existing time correction"
            else:
                type = "Add to existing time correction"
            logtext = "\tType of Correction: " + type
            self.writeLog(logtext, "Time", "black")

            if not BATCHMODE:
                TimeTagQuest = self.TimeTagQuest_cb.currentIndex()
            else:
                TimeTagQuest = self.TimeTagQuest
            if TimeTagQuest:
                action = TimeTagQuest
                if action == "set":
                    logtext = "\t\"Time tag is questionable\" flag set"
                elif action == "unset":
                    logtext = "\t\"Time tag is questionable\" flag unset"
                self.writeLog(logtext, "Time", "black")

            # set up dictionary to monitor corrected files
            if key not in self.TimeCorrectedFiles:
                self.TimeCorrectedFiles[key] = []
            # apply corrections for selected keys
            textstr = "Correcting File Number: "
            for timetup in self.TraceTimeDict[key][1:]:
                if self.UseNewOnlyTime:
                    if timetup[0] in self.TimeCorrectedFiles[key]:
                        skip_cnt += 1
                        infotext = "Skipping File " + str(skip_cnt)
                        self.addTextInfoBar(infotext)
                        continue
                # respond to cancel requests
                if not self.RunApplyTimeCor:
                    infotext = "Canceled Applied Corrections to: " + \
                        str(num_cnt) + " files."
                    self.addTextInfoBar(infotext, 'orange')
                    logtext = "\t" + infotext
                    self.writeLog(logtext, "Time", "red")
                    break
                if (startEpoch <= timetup[1] and timetup[1] <= endEpoch) or \
                   (startEpoch <= timetup[2] and timetup[2] <= endEpoch) or \
                   (timetup[1] <= startEpoch and endEpoch <= timetup[2]):
                    # info for user
                    if not self.timeSet(timetup[0], startEpoch, endEpoch):
                        err_cnt += 1
                        # if this fails, make sure UpdateTimeDict is cleared
                        # for key
                        del self.UpdateTimeDict[key]
                        break
                    else:
                        self.TimeCorrectedFiles[key].append(timetup[0])
                        num_cnt += 1
                        if key not in self.SetTimeDict:
                            self.SetTimeDict[key] = {}
                    if mod(num_cnt, 10):
                        pass
                    else:
                        if not BATCHMODE:
                            self.funcWorker.update.emit(num_cnt)
                        else:
                            self.wait(textstr, num_cnt)

            # keep track of time corrections applied
            if key in self.UpdateTimeDict:
                for subkey in self.UpdateTimeDict[key]:
                    # add to existing if TypeTimeCor is set to 'Add To' i.e.
                    # == 0
                    if subkey == "Time_Shift_sec" \
                       and not self.TypeTimeCor:
                        # add to previous if the user ignored warning to Apply
                        # unapplied only. the user can totally screw this up
                        # since the time shift is for a Sta:Loc:Net and not
                        # by trace. e.g. consecutive runs of ApplyTimeCor
                        # sometimes Applying all corrections
                        # and other applying only unapplied.
                        if subkey in self.SetTimeDict[key] \
                           and not self.UseNewOnlyTime:
                            self.SetTimeDict[key][subkey] = \
                                float(self.SetTimeDict[key][subkey]) + \
                                float(self.UpdateTimeDict[key][subkey])
                        else:
                            self.SetTimeDict[key][subkey] = \
                                self.UpdateTimeDict[key][subkey]
                    else:
                        self.SetTimeDict[key][subkey] = \
                            self.UpdateTimeDict[key][subkey]

        if err_cnt:
            logtext = "\tTime corection failed for " + str(err_cnt) + " files"
            self.writeLog(logtext, "Time", "red")

        if self.RunApplyTimeCor:
            infotext = "Done. Applied Corrections to: " + str(num_cnt) + \
                " files."
            self.addTextInfoBar(infotext, 'green')
            # if not BATCHMODE:
            #    self.killshowCancelTL(self.RunApplyTimeCor)
            logtext = "\t" + infotext
            self.writeLog(logtext, "Time", "green")
        self.ActiveText = ""
        # self.writeLog("<end>\n", "Time", "blue")

        # some cleanup
        self.writeLog("<end>", "Time", "blue")
        if not BATCHMODE:
            self.resetTimeShift()
        self.IgnoreWarn = 0
        self.UseNewOnlyTime = 0

########################################

    def launchUndoTimeCor(self):
        """
        Launch UndoTimeCor in thread
        """
        if not self.SetTimeDict:
            infotext = "No Time Corrections Applied this session"
            self.addTextInfoBar(infotext, 'orange')
            return

        # if headers have already been alterred warn user
        cancel = self.showWarnTL("undo")
        if cancel:
            return

        self.resetTimeShift()
        self.UpdateTimeDict = {}
        for key in self.SetTimeDict:
            # Populate UpdateTimeDict with previously applied Time Corrections
            if key not in self.UpdateTimeDict:
                self.UpdateTimeDict[key] = {}
            for subkey in self.SetTimeDict[key]:
                if subkey == "Time_Shift_sec":
                    self.UpdateTimeDict[key][subkey] = \
                        (-1.0) * float(self.SetTimeDict[key][subkey])
                elif subkey == "Time_Tag_Quest":
                    if self.SetTimeDict[key][subkey] == "set":
                        self.UpdateTimeDict[key][subkey] = "unset"
                    elif self.SetTimeDict[key][subkey] == "unset":
                        self.UpdateTimeDict[key][subkey] = "set"
                    else:
                        self.UpdateTimeDict[key][subkey] = ""
                elif subkey == "Corr_Type":
                    self.UpdateTimeDict[key][subkey] = 0
                else:
                    self.UpdateTimeDict[key][subkey] = \
                        self.SetTimeDict[key][subkey]

        # self.SetTimeDict={}
        self.launchApplyTimeCor(1)

########################################

    def buildEpoch(self, input_time):
        """
        Build epoch time from input time string
        """
        EpochTime = None
        err = ""
        intTime = str(input_time)

        try:
            TimeStr = time.strptime(intTime, '%Y:%j:%H:%M:%S')
            EpochTime = time.mktime(TimeStr)
        except Exception as e:
            err = "Invalid Time String"
            self.ActiveText = ""
            self.AlteredText = ""
            return EpochTime, err

        return EpochTime, err

########################################

    def timeSet(self, ifile, startEpoch, endEpoch):
        """
        Apply time correction to given mseed file
        """
        # first test to see if time shift will exceed field size
        ShiftErr = self.testTimeShift()
        if ShiftErr:
            self.addTextInfoBar(ShiftErr, "red")
            self.writeLog(ShiftErr, "Time", "red")
            return 0

        # open mseed files for correction
        msfile = Mseed(ifile)
        filesize = msfile.filesize
        blksize = msfile.blksize
        (numblocks, odd_size) = divmod(filesize, blksize)

        # loop through file applying correction
        blk_err = 0
        n = 0

        if not BATCHMODE:
            if self.WildLoc.isChecked():
                WildLoc = 1
            else:
                WildLoc = 0
            TimeShiftName = self.TimeShiftName_cb.currentText()
        else:
            WildLoc = self.WildLoc
            TimeShiftName = self.TimeShiftName

        while n < numblocks:

            hdrs = msfile.fixedhdr(n * blksize)

            # more test just incase, this will fail if headers have been
            # change w/o updating the db
            (SeqNum, DHQual, res, Stat, Loc, Chan, Net) = hdrs[0]
            Stat = Stat.strip().decode()
            if WildLoc:
                Loc = "*"
            else:
                Loc = Loc.strip().decode()
            Net = Net.strip().decode()
            key = Stat + ":" + Loc + ":" + Net
            if key != TimeShiftName:
                blk_err += 1
                n += 1
                continue

            # determine time of blockette and process if between startEpoch
            # & endEpoch
            (Year, Day, Hour, Min, Sec, junk, Micro) = hdrs[1]
            timestr = ":".join(map(str, (Year, Day, Hour, Min, Sec)))
            time_str = time.strptime(timestr, '%Y:%j:%H:%M:%S')
            blkEpoch = time.mktime(time_str) + (Micro * 0.0001)
            if startEpoch <= blkEpoch and blkEpoch <= endEpoch:
                pass
            else:
                n += 1
                continue

            oldshift = hdrs[3][4] * 0.0001
            shift_applied = int(hdrs[3][0][6])
            # if type=replace (i.e. 1), remove oldtime shift from Epoch
            # and then apply new shift value in header equals new shift
            if not BATCHMODE:
                TSShift = float(self.TSShift_le.text())
            else:
                TSShift = self.TSShift
            if self.TypeTimeCor:
                # if an old shift exists and is applied
                if shift_applied and oldshift:
                    blkEpoch = blkEpoch + TSShift - oldshift
                    newshift = TSShift
                # if an old shift  shift exists and is not applied
                elif not shift_applied and oldshift:
                    blkEpoch = blkEpoch + TSShift
                    newshift = TSShift
                else:
                    blkEpoch = blkEpoch + TSShift
                    newshift = TSShift
            else:
                # if type=add (i.e. 0), apply new shift to Epoch
                # shift value in header equals old + new shift
                # if an old shift  shift exists and is applied
                if shift_applied and oldshift:
                    newshift = TSShift + oldshift
                    blkEpoch = blkEpoch + TSShift
                # if an old shift  exists and is not applied
                elif not shift_applied and oldshift:
                    newshift = TSShift + oldshift
                    blkEpoch = blkEpoch + TSShift + oldshift
                else:
                    newshift = TSShift
                    blkEpoch = blkEpoch + TSShift

            hdrs[3][4] = int(newshift / 0.0001)
            EpochInt = int(blkEpoch)
            EpochDec = (blkEpoch - EpochInt)
            newMicro = round(EpochDec / 0.0001)

            # build new start time entry
            (year, month, day, hour, min, sec, wkday, jday, dst) = \
                time.gmtime(EpochInt)

            # set/unset Activity flag to indicate time shift
            if newshift:
                hdrs[3][0] = self.setFlag(hdrs[3][0], 1)
            else:
                hdrs[3][0] = self.setFlag(hdrs[3][0], 1, "unset")

            # set/unset Data Quality flag to indicate "Time tag questionable"
            if not BATCHMODE:
                TimeTagQuest = self.TimeTagQuest_cb.currentText()
            else:
                TimeTagQuest = self.TimeTagQuest
            if TimeTagQuest:
                action = TimeTagQuest
                hdrs[3][2] = self.setFlag(hdrs[3][2], 7, action)

            newhdrs = (hdrs[0], (year, jday, hour, min, sec, junk, newMicro),
                       hdrs[2], hdrs[3])

            bytes_written = msfile.WriteFixedHdr(newhdrs, n * blksize)

            # This block of code should be activated. What needs to happen
            # is that for every blockette that has a BTime entry, that entry
            # should be shifted based on the above logic.
            # numblk=hdrs[3][3]
            # addseek=hdrs[3][6]
            # b=0
            # loop over number of blockettes following fixed header at block n
            # I should be searching for blk 500 here.
            # while b < numblk:
            # seekval=(n*blksize)+addseek
            # (blktype, next)=msfile.typenxt(seekval)
            # print blktype, next
            # getBlkCmd="blklist=msfile.blk" + str(blktype) + "(" +
            #    str(seekval) + ")"
            # exec getBlkCmd
            # addseek=next
            # b+=1
            n += 1
        # get endianess before closing
        msfile.close()

        # some user info
        if blk_err:
            err = str(blk_err) + " blockettes had the wrong key in file: " + \
                ifile
            self.writeLog(err, "Time", "red")
            err = "\tNO CORRECTION APPLIED TO THESE BLOCKETTES"
            self.writeLog(err, "Time", "red")
            if blk_err == numblocks:
                return 0
            # clear variables etc.
        return 1

########################################

    def testTimeShift(self):
        """
        Test if time correction exceeds field
        At this point this is a very brute test. The second half of the code
        (presently commented) tests each blockette for time corrections and
        then determines if the resultant correction will exceed the field. I
        believe that this is un-necessary and definitely adds time.
        """
        # if replacing existing correction we just need to test if entered time
        # exceeds field size
        # convert sec to 0.0001 sec
        # if self.TypeTimeCor.get():
        if not BATCHMODE:
            TSShift = float(self.TSShift_le.text())
        else:
            TSShift = self.TSShift
        NewShiftmSec = TSShift / 0.0001
        if -2**31 >= NewShiftmSec or NewShiftmSec >= (2**31) - 1:
            err = "Time Correction exceeds limits of field [-2**31 <= "\
                "TimeCorr <= (2**31)-1]"
            return err
        # if adding to existing we need to test all corrections for a given
        # file to see if the resultant correction exceeds field size
        # else:
            # open mseed file
            # try:
            # msfile=Mseed(ifile)
            # (filesize,blksize)=msfile.sizes()
            # initialize variables, lists, and dicts for later
            # filesize=msfile.filesize
            # blksize=msfile.blksize
            # (numblocks, odd_size)=divmod(filesize, blksize)
            # except Exception as e:
            # err= "Corrupt mseed: " + ifile
            # msfile.close()
            # return err
            # loop through file, snarf time corrections and test for size
            # blk_err=0
            # n=0
            # while n < numblocks:
            # hdrs=msfile.fixedhdr(n*blksize)
            # oldshift= hdrs[3][4] * 0.0001
            # if an old shift exists and is applied
            # if oldshift:
            # newshift = self.TSShift.get() + oldshift
            # else:
            # newshift = self.TSShift.get()

            # NewShiftmSec = newshift/0.0001
            # if -2**31 >= NewShiftmSec or NewShiftmSec >=
            # (2**31)-1 :
            # err= "Time Correction exceeds limits of
            # field [-2**31 <= TimeCorr <= (2**31)-1]"
            # return err
            # n+=1
            # msfile.close()
        return 0

########################################

    def loadTimeSelect(self):
        """
        Loads info for selected stat:loc:net key
        """
        key = self.TimeShiftName_cb.currentText()
        self.clearTS()
        self.AddToTimeCor_rb.setChecked(True)
        """
        self.TimeTagQuest_cb.setCurrentIndex(0)
        self.TSStartTime_le.setText("")
        self.TSEndTime_le.setText("")
        self.TSShift_le.setText("")
        """

        # modify self.TimeShiftName if user wants wildcard
        if self.WildLoc.isChecked():
            self.wildCardLoc()

        # load info if Dictionary already has entry for selection
        if key in self.UpdateTimeDict:
            for var in self.ShiftVars:
                if var[0] in self.UpdateTimeDict[key]:
                    if isinstance(var[1], QLineEdit):
                        var[1].setText(str(self.UpdateTimeDict[key][var[0]]))
                    elif isinstance(var[1], QComboBox):
                        var[1].setCurrentText(self.UpdateTimeDict[key][var[0]])
                    else:
                        # int
                        var[1] = (self.UpdateTimeDict[key][var[0]])

            # test to see if time corrections have been applied.
            # If so, do they match dictionary?
            # if so, replace variable sets from above step
            if key in self.SetTimeDict:
                for var in self.ShiftVars:
                    if var[0] in self.SetTimeDict[key]:
                        if isinstance(var[1], QLineEdit):
                            var[1].setText(str(self.SetTimeDict[key][var[0]]))
                        elif isinstance(var[1], QComboBox):
                            var[1].setCurrentText(
                                self.SetTimeDict[key][var[0]])
                        else:
                            # int
                            var[1] = (self.SetTimeDict[key][var[0]])
                if self.TSShift_le.text():
                    self.SetTime.setChecked(True)
                if self.TimeTagQuest_cb.currentText():
                    self.SetTQuest.setChecked(True)
        else:
            self.getStartEnd()

########################################

    def recalcStartEnd(self):
        """
        Recalculates start and endtimes for traces for a given
        stat:loc:net key
        """
        timeid = self.TimeShiftName_cb.currentText()
        # clear entry if time is already calculated and Traces loaded
        if timeid in self.TraceTimeDict:
            del self.TraceTimeDict[timeid]
        self.getStartEnd()

########################################

    def getStartEnd(self):
        """
        Gets start and endtimes for traces for a given stat:loc:net key
        """
        if not BATCHMODE:
            timeid = self.TimeShiftName_cb.currentText()
        else:
            timeid = self.TimeShiftName
        if not timeid:
            self.addTextInfoBar("No Sta:Loc:Net selected", 'orange')
            return

        # don't spend time here if time is already calculated
        # and Traces loaded
        if timeid in self.TraceTimeDict:
            try:
                if self.TraceTimeDict[timeid][0]:
                    # see if begin and end times exist
                    self.fillTime(timeid)
                    return
            except Exception as e:
                pass

        textstr = "Examining traces for key: " + timeid
        self.addTextInfoBar(textstr, 'lightblue')

        (keystat, keyloc, keynet) = timeid.split(":")
        cnt = 0
        for key in self.TraceDict:
            (stat, chan, loc, net, sps) = key.split(":")
            if keystat == stat and keynet == net and \
               (keyloc == loc or keyloc == "*"):
                for infile in self.TraceDict[key]:
                    cnt += 1
                    if mod(cnt, 10):
                        pass
                    else:
                        textstr = "Key: " + timeid + " Examining Trace: "
                        self.wait(textstr, cnt)

                    # at this point all files in TraceDict should be mseed
                    # files, so not testing here
                    msfile = Mseed(infile)
                    try:
                        (begintime, endtime) = msfile.FirstLastTime()
                        if cnt == 1:
                            # and [begintime,endtime] not in
                            # self.TraceTimeDict[timeid]:
                            self.TraceTimeDict[timeid] = []
                            self.TraceTimeDict[timeid].append(
                                [begintime, endtime])
                            self.TraceTimeDict[timeid].append(
                                [infile, begintime, endtime])
                        else:
                            if not [infile, begintime, endtime] \
                               in self.TraceTimeDict[timeid]:
                                if begintime < self.TraceTimeDict[timeid][0][0]:  # noqa: E501
                                    self.TraceTimeDict[timeid][0][0] = \
                                        begintime
                                if endtime > self.TraceTimeDict[timeid][0][1]:
                                    self.TraceTimeDict[timeid][0][1] = endtime
                                self.TraceTimeDict[timeid].append(
                                    [infile, begintime, endtime])
                    except Exception as e:
                        logtext = "Corrupt mseed file: " + infile
                        self.writeLog(logtext, 'Time', 'red')
                    msfile.close()
            else:
                continue
        if timeid in self.TraceTimeDict:
            try:
                self.fillTime(timeid)
            except Exception as e:
                err = "Cannot determine begin and end times for " + timeid + \
                    ". See log"
                self.addTextInfoBar(err, 'red')

########################################

    def fillTime(self, keyname):
        """
        Fills time and count for Time Shift nb
        """
        startepoch = self.TraceTimeDict[keyname][0][0]
        # decided not to used micro sec accuracy since round off errors
        # are creating problems
        # micro= int((startepoch - int(startepoch)) / 0.0001)
        # micro=str(micro)
        # numz=4-len(micro)
        # micro=numz*"0" + micro
        start = time.strftime('%Y:%j:%H:%M:%S', time.gmtime(startepoch))
        # start=start + "." + str(micro)
        if not BATCHMODE:
            self.TSStartTime_le.setText(start)
        else:
            self.TSStartTime = start

        endepoch = self.TraceTimeDict[keyname][0][1]
        # to make sure we pickup the last block
        endepoch += 1
        # decided not to used micro sec accuracy since round off errors
        # are creating problems
        # micro= int((endepoch - int(endepoch)) / 0.0001)
        # micro=str(micro)
        # numz=4-len(micro)
        # micro=numz*"0" + micro
        end = time.strftime('%Y:%j:%H:%M:%S', time.gmtime(endepoch))
        # end=end + "." + str(micro)
        if not BATCHMODE:
            self.TSEndTime_le.setText(end)
        else:
            self.TSEndTime = end
        # first record has min/max time for keyname
        self.timeCnt = len(self.TraceTimeDict[keyname]) - 1
        textstr = str(self.timeCnt) + " traces selected for key: " + keyname
        self.addTextInfoBar(textstr, 'lightblue')

########################################

    # wasn't called so commented out
    # def mkNumChar(self, instr, num):
    #    """
    #    appends num-len(instr) 0 to input string
    #    or truncates string to num chars
    #    """
    #
    #    if len(instr) < num:
    #        m = num - len(instr)
    #        return instr + m * "0"
    #    elif len(instr) > num:
    #        return instr[0:num]
    #    else:
    #        return instr

########################################

    def writeLog(self, info, type="", color="black"):
        """
        Writes text to log notebook
        """
        if BATCHMODE:
            if info:
                print(info)
        else:
            # local variables
            LogAllappend = self.LogAll.append
            LogHeaderappend = self.LogHeader.append
            LogTimeappend = self.LogTime.append
            LogEndianappend = self.LogEndian.append

            LogAllappend((info, color))
            self.LogStack.widget(0).clear()
            textlist = self.LogAll
            for (textcmd, color) in textlist:
                self.AllText.setTextColor(color)
                self.AllText.append(textcmd)

            if type == "Header":
                LogHeaderappend((info, color))
                self.LogStack.widget(1).clear()
                textlist = self.LogHeader
                for (textcmd, color) in textlist:
                    self.HeaderText.setTextColor(color)
                    self.HeaderText.append(textcmd)
            elif type == "Time":
                LogTimeappend((info, color))
                self.LogStack.widget(2).clear()
                textlist = self.LogTime
                for (textcmd, color) in textlist:
                    self.TimingText.setTextColor(color)
                    self.TimingText.append(textcmd)
            elif type == "Endian":
                LogEndianappend((info, color))
                self.LogStack.widget(3).clear()
                textlist = self.LogEndian
                for (textcmd, color) in textlist:
                    self.EndianText.setTextColor(color)
                    self.EndianText.append(textcmd)

########################################

    def displayLog(self):
        """
        Set which log entries to display
        """
        if self.All.isChecked():
            self.LogStack.setCurrentIndex(0)
        elif self.Header.isChecked():
            self.LogStack.setCurrentIndex(1)
        elif self.Timing.isChecked():
            self.LogStack.setCurrentIndex(2)
        elif self.Endian.isChecked():
            self.LogStack.setCurrentIndex(3)

########################################

    def flushLogs(self):
        """
        Flushes logs
        """
        self.LogAll = []
        self.LogHeader = []
        self.LogTime = []
        self.LogEndian = []
        self.LogSummary = []

        for i in range(0, self.LogStack.count()):
            self.LogStack.widget(i).clear()

########################################

    def launchChangeEndian(self, endian):
        """
        Separated out so ChangeEndian could be run as a thread
        """
        self.RunChangeEndian = 1

        if endian == "big":
            self.ToEndian = "big"
            title = "Change Endian To Big"
        else:
            self.ToEndian = "little"
            title = "Change Endian To Little"
        # launch thread

        # run as thread
        # func = lambda: self.changeEndian()
        self.initThread(title, self.changeEndian)

########################################

    def changeEndian(self):
        """
        Rewrites all blockettes in either big or little endian
        """
        self.ActiveText = "Changing Endianess"
        endian = self.ToEndian
        if endian == "big":
            inlist = self.LittleEndianList
            outlist = self.BigEndianList
            numfiles = len(self.LittleEndianList)
            if not numfiles:
                err = "No Little Endian Files Found"
                self.addTextInfoBar(err, "red")
                return
        else:
            inlist = self.BigEndianList
            outlist = self.LittleEndianList
            numfiles = len(self.BigEndianList)
            if not numfiles:
                err = "No Big Endian Files Found"
                self.addTextInfoBar(err, "red")
                return

        files_left = numfiles
        if BATCHMODE:
            textstr = "Files Remaining: "
            self.wait(textstr, files_left)
        else:
            self.addTextInfoBar()

        # write log and user information
        self.writeLog("\n<Change Endianess>", "Endian", "blue")
        logtext = "\tSelected " + str(numfiles) + " traces to convert to " + \
            endian + " endian headers"
        self.writeLog(logtext, "Endian", "black")

        # loop over files reading & writing blockettes
        corr_files = 0
        while inlist:
            ErrFlag = 0
            # respond to cancel requests
            if not self.RunChangeEndian:
                break
            ifile = inlist.pop()
            #         info for user
            if mod(files_left, 5):
                pass
            else:
                if BATCHMODE:
                    self.wait(textstr, files_left)
                else:
                    self.funcWorker.update.emit(corr_files)
            files_left -= 1

            msfile = Mseed(ifile)
            # local variables'
            fixedhdr = msfile.fixedhdr
            WriteFixedHdr = msfile.WriteFixedHdr
            typenxt = msfile.typenxt
            GetBlk = msfile.GetBlk
            PutBlk = msfile.PutBlk

            filesize = msfile.filesize
            blksize = msfile.blksize
            (numblocks, odd_size) = divmod(filesize, blksize)
            outlist.append(ifile)

            blk_err = 0
            n = 0
            while n < numblocks:
                # read & rewrite fixed header
                hdrs = fixedhdr(n * blksize)
                bytes_written = WriteFixedHdr(hdrs, n * blksize, endian)
                numblk = hdrs[3][3]
                addseek = hdrs[3][6]
                b = 0
                # loop over number of blockettes following fixed header at
                # block n
                while b < numblk:
                    seekval = (n * blksize) + addseek
                    (blktype, next) = typenxt(seekval)
                    blklist = GetBlk(blktype, seekval)
                    if not blklist:
                        print("not blklist")
                        # if blockette error encountered bail and undo endian
                        # changes
                        err = "\n\tCorrupt mseed: " + ifile
                        self.writeLog(err, "Endian", "red")
                        err = "\t\tFile removed from Trace db"
                        self.writeLog(err, "Endian", "red")
                        err = "\t\tRecord: " + str(n)
                        self.writeLog(err, "Endian", "red")
                        err = "\t\tUnrecognized Blockette: " + str(blktype)
                        self.writeLog(err, "Endian", "red")
                        msfile.close()
                        self.undoEndian(ifile, n, b, endian)
                        # remove file from list
                        outlist.pop()
                        # set error flag
                        ErrFlag = 1
                        # ensure outer while block will end
                        n += numblocks
                        break
                    else:
                        try:
                            bytes_written = PutBlk(
                                blktype, blklist, endian, seekval)

                        except Exception as e:
                            # WriteBlk function not found
                            # undo endian changes
                            err = ("\n\tError at file " + str(ifile) +
                                   ":\n\tself.Writeblk")
                            err += (str(blktype) + " not in LibTrace.py")
                            self.writeLog(err, "Endian", "red")
                            msfile.close()
                            self.undoEndian(ifile, n, b, endian)
                            # remove file from list
                            outlist.pop()
                            # set error flag
                            ErrFlag = 1
                            # ensure outer while block will end
                            n += numblocks
                            break
                    addseek = next
                    b += 1
                n += 1
            if not ErrFlag:
                corr_files += 1
                msfile.close()

        # flush endian list to prevent redoing process
        if endian == "big":
            self.LittleEndianList = inlist
            self.BigEndianList = outlist
        else:
            self.BigEndianList = inlist
            self.LittleEndianList = outlist

        self.NumBigFiles = len(self.BigEndianList)
        self.NumLittleFiles = len(self.LittleEndianList)

        if not BATCHMODE:
            self.NumBig_le.setText(str(self.NumBigFiles))
            self.NumLittle_le.setText(str(self.NumLittleFiles))

        # write log and user information
        logtext = "\n\tConverted " + str(corr_files) + " traces to " + \
            endian + " endian headers"
        self.writeLog(logtext, "Endian", 'black')
        self.writeLog("<end>", "Endian", 'blue')

        infotext = "\nDone. Converted: " + str(corr_files) + " files."
        self.addTextInfoBar(infotext, 'green')

        self.ActiveText = ""

########################################

    def undoEndian(self, infile, recnum, blknum, endian):
        """
        changes a corrupt mseed file back to original endianess
        after ChangeEndian catches problem while reading file.
        This approach is taken rather than scanning entire file first
        that would take more time for non-problematic files.
        """
        msfile = Mseed(infile)
        # (filesize,blksize)=msfile.sizes()
        filesize = msfile.filesize
        blksize = msfile.blksize

        n = 0
        # reverse endian
        if endian == "big":
            endian = "little"
        else:
            endian = "big"
        # loop upto position that gave a problem
        while n <= recnum:
            # read & rewrite fixed header
            hdrs = msfile.fixedhdr(n * blksize)
            bytes_written = msfile.WriteFixedHdr(hdrs, n * blksize, endian)
            numblk = hdrs[3][3]
            addseek = hdrs[3][6]
            b = 0
            # correct upto the blockette that gave problems
            while b < blknum:
                seekval = (n * blksize) + addseek
                (blktype, next) = msfile.typenxt(seekval)
                blklist = msfile.GetBlk(blktype, seekval)
                bytes_written = msfile.PutBlk(
                    blktype, blklist, endian, seekval)
                addseek = next
                b += 1
            n += 1
        msfile.close()

########################################

    def setFlag(self, instr, position, action="set"):
        """
        Sets/unsets flag in string representation of byte
        """
        newstr = ""
        b = len(instr) - 1
        for a in instr:
            if (b) == position:
                if action == "set":
                    a = "1"
                else:
                    a = "0"
            newstr = newstr + a
            b -= 1
        return newstr

########################################

    def listTrace(self):
        """
        Provide a toplevel window listing traces for a given key
        """
        if not self.StationName_cb.currentText():
            self.addTextInfoBar(
                "No Sta:Chan:Loc:Net:Sps selected", 'orange')
        else:
            self.childTL = self.TopLevel("List Traces", self)
            self.childTL.show()

########################################

    def templateFormat(self):
        """
        Template file format help and examples
        """
        self.childTL = self.TopLevel("Template Format", self)
        self.childTL.show()

########################################

    def listTimeCor(self):
        """
        Provide a toplevel window listing time corrections in fixed headers
        of traces for a given key
        """
        if not self.TimeShiftName_cb.currentText():
            self.addTextInfoBar("No Sta:Loc:Net selected", 'orange')
            return

        self.RunListTimeCor = 1
        self.initTimeCorWin()
        self.initThread("List Time Corrections",
                        self.updateTimeCorWin,
                        self.afterUpdateTLC)

########################################

    def getTimeCor(self):
        """
        Get time corrections from trace headers
        """
        # local variables
        FileCorDict = {}
        timeid = self.TimeShiftName_cb.currentText()
        (keystat, keyloc, keynet) = timeid.split(":")
        cnt = 0
        for key in self.TraceDict:
            (stat, chan, loc, net, sps) = key.split(":")
            if keystat == stat and keynet == net and \
               (keyloc == loc or keyloc == "*"):
                for infile in self.TraceDict[key]:
                    # handle interrupt request
                    if not self.RunListTimeCor:
                        break
                    if mod(cnt, 10):
                        pass
                    else:
                        # self.TimeCorListText.insert(
                        # "end", """%s\n""" % timeid)
                        textstr = "Key: " + timeid + " Examining Trace: "
                        # self.wait(textstr, cnt)
                        self.funcWorker.update.emit(cnt)
                    cnt += 1

                    # at this point all files in TraceDict should be mseed
                    # files, so not testing here
                    msfile = Mseed(infile)
                    fixedhdr = msfile.fixedhdr  # local var

                    filesize = msfile.filesize
                    blksize = msfile.blksize
                    (numblocks, odd_size) = divmod(filesize, blksize)

                    n = 0
                    while n < numblocks:
                        hdrs = msfile.fixedhdr(n * blksize)
                        (Year, Day, Hour, Min, Sec, junk, Micro) = hdrs[1]
                        Day = self.addZero(Day, 3)
                        Hour = self.addZero(Hour, 2)
                        Min = self.addZero(Min, 2)
                        Sec = self.addZero(Sec, 2)
                        hdtime = ":".join(map(str, (
                            Year, Day, Hour, Min, Sec)))

                        (act, io, DQual, numblk, timcorr, bdata, bblock) = \
                            hdrs[3]
                        # has time correction been applied
                        if int(act[6]):
                            corr = "applied"
                        else:
                            corr = "not applied"
                        # is timing questionable flag set
                        if hdrs[3][2][0] == "1":
                            timequest = "set"
                        else:
                            timequest = "unset"

                        timcorr = float(timcorr) / 10000.
                        try:
                            FileCorDict[infile].append((
                                hdtime, timcorr, corr, timequest))
                        except KeyError:
                            FileCorDict[infile] = []
                            FileCorDict[infile].append((
                                hdtime, timcorr, corr, timequest))

                        n += 1
                    msfile.close()
            else:
                continue
        return FileCorDict

########################################

    def addZero(self, var, num):
        """
        Pad header values to specified length with blank space
        right justified
        """
        var = str(var)
        varlength = len(var)
        if varlength == num:
            return var
        elif varlength < num:
            pad = num - varlength
            newvar = "0" * pad + var
            return newvar

########################################

    def dumpUpdateDicts(self, type=""):
        """
        Dump UpdateHdrDict to Log notebook
        """
        if not BATCHMODE:
            # make sure any current window edits get added
            if self.StationName_cb.currentText():
                self.updateHdr(self.StationName_cb.currentText())
                # self.timeDictSet()

            if not self.UpdateHdrDict and not self.UpdateTimeDict:
                err = "No updates to dump."
                self.addTextInfoBar(err, 'red')
                return

        if type == "dump" or type == "load" or type == "mod":
            applied = "\tapplied"
            not_applied = "\tnot applied"
        else:
            applied = "saved"
            not_applied = "saved"

        # set color of entry based on whether applied or not
        if self.UpdateHdrDict:
            if type == "dump":
                self.writeLog("\n<Dump Update Dictionary>", "Header", "blue")
            inlist = sorted(list(self.UpdateHdrDict.keys()))

            for key in inlist:
                self.writeLog(key, "Header", "black")
                for subkey in self.UpdateHdrDict[key]:
                    if not BATCHMODE:
                        # if location code cleared
                        if self.UpdateHdrDict[key][subkey] == " ":
                            value = "\t" + subkey + "\t" + \
                                "CLEAR"
                        else:
                            value = "\t" + subkey + "\t" + \
                                self.UpdateHdrDict[key][subkey]
                        addsp = 0
                        space = 0
                    else:
                        value = "\t" + subkey + SPACE + \
                            self.UpdateHdrDict[key][subkey]
                        addsp = 25 - len(value)
                        space = SPACE
                    if key in self.SetHdrDict:
                        if subkey in self.SetHdrDict[key]:
                            value = value + SPACE * addsp + applied
                            tset = 'black'
                        else:
                            value = value + SPACE * addsp + not_applied
                            tset = 'red'
                    else:
                        value = value + SPACE * addsp + not_applied
                        tset = 'red'
                    # insert info on Log notebook
                    self.writeLog(value, "Header", tset)
            if type == "dump":
                self.writeLog("<end>", "Header", "blue")

        if self.UpdateTimeDict:
            if type == "dump":
                self.writeLog("\n<Dump Update Dictionary>", "Time", "blue")
            inlist = sorted(list(self.UpdateTimeDict.keys()))
            for key in inlist:
                self.writeLog(key, "Time", "black")
                for var in self.ShiftVars:
                    if var[0] in self.UpdateTimeDict[key]:
                        value = "\t" + var[0]
                        if len(str(self.UpdateTimeDict[key][var[0]])) == 1:
                            value = value + "\t\t\t" + \
                                str(self.UpdateTimeDict[key][var[0]])
                        else:
                            value = value + "\t\t" + \
                                str(self.UpdateTimeDict[key][var[0]])
                        if key in self.SetTimeDict:
                            if var[0] in self.SetTimeDict[key]:
                                value = value + "\t" + applied
                                tset = 'black'
                            else:
                                value = value + "\t" + not_applied
                                tset = 'red'
                        else:
                            value = value + "\t" + not_applied
                            tset = 'red'
                        # insert info on Log notebook
                        self.writeLog(value, "Time", tset)
            if type == "dump":
                self.writeLog("<end>", "Time", "blue")

########################################

    def exit(self):
        """
        a no questions asked exit
        """
        sys.exit(0)

########################################

    def exitCheck(self):
        """
        before exitting test to see if any UpdateHdrDict values
        have not yet been applied and allow user to apply before
        exiting
        """
        self.updateHdr()

        for var in self.UpdateHdrDict:
            self.addTextInfoBar(" " + var + "...", 'orange')
            if var not in self.SetHdrDict:
                self.exitWidget()
                return
            else:
                for subkey in self.UpdateHdrDict[var]:
                    if subkey not in self.SetHdrDict[var]:
                        self.exitWidget()
                        return
        self.exit()

########################################

    def exitWidget(self):
        """
        Allow user to apply UpdateHdrDict entries that have not
        been applied
        """
        WarnMsg = QMessageBox()
        WarnMsg.setIcon(QMessageBox.Warning)
        WarnMsg.setWindowTitle("Warning")

        infotext = "WARNING:\nNot All Entries Have Been Applied."

        WarnMsg.setText(infotext)
        cancel_b = WarnMsg.addButton("Cancel", QMessageBox.NoRole)
        exit_b = WarnMsg.addButton("Exit", QMessageBox.YesRole)

        cancel_b.setStyleSheet(
            "QAbstractButton::hover{background-color:orange;}")
        exit_b.setStyleSheet(
            "QAbstractButton::hover{background-color:red;}"
        )

        WarnMsg.exec_()

        if (WarnMsg.clickedButton() == cancel_b):
            return
        else:
            self.exit()

########################################

    def resetTimeShift(self):
        """
        Set vars to display input format
        """
        self.AddToTimeCor_rb.setChecked(True)
        self.TSShift_le.setText("0.0")
        self.TimeTagQuest_cb.setCurrentIndex(0)
        self.TimeShiftName_cb.blockSignals(True)
        self.TimeShiftName_cb.setCurrentIndex(0)
        self.TimeShiftName_cb.blockSignals(False)
        self.TSStartTime_le.setText("yyyy:ddd:hh:mm:ss")
        self.TSEndTime_le.setText("yyyy:ddd:hh:mm:ss")
        self.SetTime.setChecked(False)
        self.SetTQuest.setChecked(False)
        self.WildLoc.setChecked(False)

########################################

    def clearTH(self):
        """
        Clear line-edits, checkboxes and set dropdown indeces to 0
        in Trace Headers tab
        """
        for var in self.StatChanListVars:
            if var[0] == "Stat:Chan:Loc:Net:Sps":
                var[1].setCurrentIndex(0)
            else:
                var[1].clear()

        for var in self.NewHdrVars:
            var[1].clear()
            var[2].setChecked(False)

        self.addTextInfoBar()

########################################

    def clearUpdateDict(self, flush=0):
        """
        Clear values in UpdateHdrDict if the associated trace
        headers have not already been applied.
        """
        if flush:
            self.UpdateHdrDict = {}
            self.SetHdrDict = {}
        else:
            try:
                del self.UpdateHdrDict[self.StationName_cb.currentText()]
                del self.SetHdrDict[self.StationName_cb.currentText()]
            except Exception as e:
                pass

        # clear entry boxes and check boxes
        self.clearTH()

########################################

    def clearGM(self):
        """
        Clear line-edits and set dropdown indeces to 0
        in Global Modify tab
        """
        for var in self.GlobalListVars:
            var[1].setCurrentIndex(0)
            var[3].clear()

        self.addTextInfoBar()

########################################

    def clearTS(self):
        """
        Clear line-edits, checkboxes and set dropdown indeces to 0
        in Time Shift tab
        """
        for var in self.ShiftVars:
            if isinstance(var[1], QLineEdit):
                var[1].clear()
            elif isinstance(var[1], QComboBox):
                var[1].setCurrentIndex(0)
            else:
                var[1] = 0

        for var in self.SetTimeVars:
            var[1].setChecked(False)

        self.addTextInfoBar()

########################################

    def clearAll(self):
        """
        Completely wipe all info from db
        """
        # clear any lingering updates
        self.UpdateHdrDict = {}
        self.SetHdrDict = {}
        self.UpdateTimeDict = {}
        self.SetTimeDict = {}

        # clear widgets
        for var in self.StatChanListVars:
            var[1].clear()

        for var in self.GlobalListVars:
            var[1].clear()
            var[2].clear()
            var[3].clear()

        for var in self.NewHdrVars:
            var[1].clear()

        for var in self.NewHdrVars:
            var[2].setChecked(False)

        for var in self.ShiftVars:
            if isinstance(var[1], int):
                var[1] = 0
            else:
                var[1].clear()

        self.addTextInfoBar()

########################################

    def setValue(self, var, value=''):
        """
        Sets Value of var
        """
        if var == "DataDirs":
            for le in self.DataDirLeList:
                le.clear()
        else:
            var = value

########################################

    def addTextInfoBar(self, str='', bg='yellow'):
        """
        Adds Text To InfoBar
        """
        if BATCHMODE:
            if str:
                if str.count('Examining') or str.count('Modifying') \
                   or str.count('Correcting') or str.count('Remaining'):
                    print("\r" + str,)
                    sys.stdout.flush()
                else:
                    print(str)
        else:
            if bg != "yellow" and bg != "lightblue":
                QApplication.beep()
            self.InfoString.setText(str)
            self.InfoString.setAlignment(Qt.AlignCenter)
            self.InfoString.setStyleSheet("background-color:" + bg)

########################################

    def wait(self, words, cnt):
        """
        Puts words plus cnt to info bar
        """
        txt = words + str(cnt)
        self.addTextInfoBar(txt, 'lightblue')

########################################

    def getPath(self):
        """
        Open file dialogue to search for mseed files
        """
        search = QFileDialog.getExistingDirectory()
        directories = self.THDataDirs_le.text()

        if search:
            if directories:
                for le in self.DataDirLeList:
                    le.insert(":" + search)
            else:
                for le in self.DataDirLeList:
                    le.insert(search)

########################################

    def showWarnTL(self, action):
        """
        Toplevel to warn user if headers have been modified
        """
        WarnMsg = QMessageBox()
        WarnMsg.setIcon(QMessageBox.Warning)
        WarnMsg.setWindowTitle("Warning")
        result = None

        if action == "undo":
            infotext = "This will only remove single and cumulative "\
                "corrections.\nReview 'List Time Corrections\' to "\
                "better understand corrections\nalready applied to "\
                "trace headers. Consider recalculating start"\
                "\nand end times before continuing"

            WarnMsg.setText(infotext)
            IgnoreAndUndo_b = WarnMsg.addButton(
                "Ignore and Undo Corrections", QMessageBox.YesRole)
            IgnoreAndUndo_b.setStyleSheet(
                "QAbstractButton::hover{background-color:orange;}")
            cancel_b = WarnMsg.addButton(
                "Cancel", QMessageBox.HelpRole)
            cancel_b.setStyleSheet(
                "QAbstractButton{background-color:red;}")
            WarnMsg.exec_()

            if (WarnMsg.clickedButton() == cancel_b):
                result = 1
            else:
                result = 0
                self.addTextInfoBar()

        # header or time
        else:
            infotext = self.AlteredText
            headerkeys = list(self.SetHdrDict.keys())
            headerkeys = "Headers Changed: " + \
                ",\n".join(map(str, headerkeys)) + "\n"
            timekeys = list(self.SetTimeDict.keys())
            timekeys = "Blockette Times Changed: " + \
                ",\n".join(map(str, timekeys))
            infotext = infotext + headerkeys + timekeys

            if action == "time":
                WarnMsg.setText(infotext)
                ApplyUnapplied_b = WarnMsg.addButton(
                    "Apply Unapplied Only", QMessageBox.YesRole)
                ApplyUnapplied_b.setStyleSheet(
                    "QAbstractButton{background-color:lightblue;}"
                    "QAbstractButton::hover{background-color:green;}")
                ApplyAll_b = WarnMsg.addButton(
                    "Apply All", QMessageBox.YesRole)
                ApplyAll_b.setStyleSheet(
                    "QAbstractButton::hover{background-color:orange;}")
                cancel_b = WarnMsg.addButton(
                    "Cancel", QMessageBox.HelpRole)
                cancel_b.setStyleSheet(
                    "QAbstractButton{background-color:red;}")

                WarnMsg.exec_()

                if (WarnMsg.clickedButton() == cancel_b):
                    result = 1
                elif (WarnMsg.clickedButton() == ApplyUnapplied_b):
                    self.newOnlyTime()
                    result = 0
                    self.addTextInfoBar()
                else:
                    result = 0
                    self.addTextInfoBar()

            else:
                WarnMsg.setText(infotext)
                ApplyNoRebuild_b = WarnMsg.addButton(
                    "Apply without rebuild", QMessageBox.YesRole)
                ApplyNoRebuild_b.setStyleSheet(
                    "QAbstractButton::hover{background-color:orange;}")
                Rebuild_b = WarnMsg.addButton(
                    "Rebuild db now", QMessageBox.YesRole)
                Rebuild_b.setStyleSheet(
                    "QAbstractButton{background-color:lightblue;}"
                    "QAbstractButton::hover{background-color:green;}")
                cancel_b = WarnMsg.addButton(
                    "Cancel", QMessageBox.HelpRole)
                cancel_b.setStyleSheet(
                    "QAbstractButton{background-color:red;}")
                WarnMsg.exec_()

                if (WarnMsg.clickedButton() == cancel_b):
                    result = 1
                elif (WarnMsg.clickedButton() == Rebuild_b):
                    self.buildTrcList()
                    result = 1
                else:
                    result = 0
                    self.addTextInfoBar()

        return result

########################################

    def saveWidget(self, type):
        """
        Toplevel to save update dictionary or
        log text to file
        """
        self.childTL = self.TopLevel(type, self)
        self.childTL.show()

        # BACHMODE var?
        # elif self.LogVar.get() == 4:
        #    textlist = self.Savefile.set("fixhdr_Summary." + now)

########################################

    def loadTemplate(self):
        """
        Find a file and import into text window
        """
        if self.BatchFile:
            selection = self.BatchFile
        else:
            selection = QFileDialog.getOpenFileName(
                parent=self, caption="Load Template",
                dir="", filter="All (*)")[0]
        if selection:
            try:
                inputfile = open(selection, "r")
                self.UpdateHdrDict = {}
                self.SetHdrDict = {}
                self.UpdateTimeDict = {}
                self.SetTimeDict = {}
                if not BATCHMODE:
                    self.clearTH()
                    self.clearTS()
                while True:
                    newline = inputfile.readline()
                    if not newline:
                        break
                    if newline[0] == "#" or newline[:-1] == "":
                        continue
                    if newline[0] == "}":
                        print("ERROR in Template File Format")
                    if newline[:7] == "hdrlist":
                        while True:
                            newline = inputfile.readline()
                            if not newline:
                                break
                            if newline[0] == "#" or newline[:-1] == "":
                                continue
                            if newline[0] == "}":
                                break
                            if len(newline[:-1].split()) != 2:
                                self.UpdateTimeDict = {}
                                break
                            try:
                                (key, newkey) = newline[:-1].split()
                            except Exception as e:
                                print("ERROR in Template File Format")
                                break
                            if key not in self.UpdateHdrDict:
                                self.UpdateHdrDict[key] = {}
                            (stat, chan, loc, net) = newkey.split(":")
                            if stat:
                                stat = stat.upper()
                                self.UpdateHdrDict[key]["Station_Name"] = stat
                            if chan:
                                chan = chan.upper()
                                self.UpdateHdrDict[key]["Channel"] = chan
                            if loc:
                                loc = loc.upper()
                                self.UpdateHdrDict[key]["Location_Code"] = loc
                            if net:
                                net = net.upper()
                                self.UpdateHdrDict[key]["Network_Code"] = net
                    elif newline[:8] == "timelist":
                        while True:
                            newline = inputfile.readline()
                            if not newline:
                                break
                            if newline[0] == "#" or newline[:-1] == "":
                                continue
                            if newline[0] == "}":
                                break
                            if len(newline[:-1].split()) != 6:
                                self.UpdateTimeDict = {}
                                break
                            (id, stime, etime, shift, quest, type) = \
                                newline[:-1].split()
                            if id not in self.UpdateTimeDict:
                                self.UpdateTimeDict[id] = {}
                            if stime:
                                self.UpdateTimeDict[id]["Start_Time"] = \
                                    stime.strip()
                            if etime:
                                self.UpdateTimeDict[id]["End_Time"] = \
                                    etime.strip()
                            if shift:
                                if shift != "NA" and shift != "na":
                                    self.UpdateTimeDict[id]["Time_Shift_sec"]\
                                        = float(shift)
                                else:
                                    self.UpdateTimeDict[id]["Time_Shift_sec"]\
                                        = 0.0
                            if quest:
                                if quest != "NA" and quest != "na":
                                    self.UpdateTimeDict[id]["Time_Tag_Quest"]\
                                        = quest.strip()
                            if type:
                                if shift != "NA" and shift != "na":
                                    if type.strip() == "add":
                                        type = 0
                                    elif type.strip() == "replace":
                                        type = 1
                                else:
                                    type = 0
                                self.UpdateTimeDict[id]["Corr_Type"] = type
                if not self.UpdateHdrDict and not self.UpdateTimeDict:
                    err = "ERROR: Template file not in correct format"
                    self.addTextInfoBar(err, 'red')
                    if BATCHMODE:
                        sys.exit(1)
                    else:
                        return
                inputfile.close()
                if not BATCHMODE:
                    # Write some log info
                    if self.UpdateHdrDict:
                        self.writeLog("\n<Load Template>", "Header", "blue")
                        self.writeLog("File: %s" % selection,
                                      "Header", "dkgreen")
                    if self.UpdateTimeDict:
                        self.writeLog("\n<Load Template>", "Time", "blue")
                        self.writeLog("File: %s" % selection,
                                      "Time", "dkgreen")
                    if self.UpdateHdrDict or self.UpdateTimeDict:
                        self.dumpUpdateDicts("load")
                    if self.UpdateHdrDict:
                        self.writeLog("<end>", "Header", "blue")
                    if self.UpdateTimeDict:
                        self.writeLog("<end>", "Time", "blue")
                info = "Load Template: %s successful" % selection
                self.addTextInfoBar(info, 'green')

            except IOError as e:
                err = "ERROR: Can't Load Template: %s" % selection, e
                self.addTextInfoBar(err, 'red')
                if BATCHMODE:
                    sys.exit(1)

########################################

    def setWidgetsVisible(self, action='hide'):
        """
        Hide widgets after app opens,
        reveal after building db
        """
        if action == "hide":
            for w in self.HideOnStartList:
                w.hide()

        else:
            for w in self.HideOnStartList:
                w.show()

########################################

    def launchFixRecordLength(self):
        """
        Separated out so FixRecordLength could be run as a thread
        """
        # no mseeds to fix
        if len(self.odd_file_list) == 0:
            del self.odd_file_list
            return

        self.RunFixRecordLength = 1
        if not BATCHMODE:
            self.initThread("Fixing Record Lenghth & Updating Trace db",
                            self.fixRecordLength,
                            self.afterBuildTrace)
        else:
            self.fixRecordLength()

    def fixRecordLength(self):
        """
        Fix mseeds with variable record length
        and add them to trace db

        Seperate from findTrace to save
        time avoid rescanning whole db
        """
        cnt = 0  # thread progress count
        NumMseedFiles = 0
        appendBigEndian = self.BigEndianList.append
        appendLittleEndian = self.LittleEndianList.append

        for file in self.odd_file_list:
            cnt += 1
            # respond to cancel request
            if not self.RunFixRecordLength:
                break
            else:
                try:
                    # make file size multiple of 4096 bytes
                    subprocess.run(
                        ['truncate', '-cs', '%4096', file],
                        check=True)
                    # print(f"Truncated: {file}")
                except subprocess.CalledProcessError as e:
                    print(f"Error truncating {file}: {e}. "
                          "Continuing to next file.")
                    continue

                if not BATCHMODE:
                    self.funcWorker.update.emit(cnt)
                else:
                    self.wait("Examining File: ", cnt)

                msfile = Mseed(file)
                # type = msfile.type
                rate = msfile.rate
                # time = msfile.time
                stat = msfile.FH.Stat.decode().strip()
                chan = msfile.FH.Chan.decode().strip()
                loc = msfile.FH.Loc.decode().strip()
                net = msfile.FH.Net.decode().strip()

                if not BATCHMODE:
                    # window on specific stations if exist
                    if self.StatSelList:
                        if stat not in self.StatSelList:
                            continue
                # build endian lists
                if msfile.byteorder == "big":
                    if file not in self.BigEndianList:
                        appendBigEndian(file)
                else:
                    if file not in self.LittleEndianList:
                        appendLittleEndian(file)

                # build file list for fixing headers
                fileid = ":".join(map(str, (
                    stat, chan, loc, net, rate)))

                try:
                    if file not in self.TraceDict[fileid]:
                        self.TraceDict[fileid].append(file)
                        NumMseedFiles += 1
                except KeyError:
                    self.TraceDict[fileid] = []
                    self.TraceDict[fileid].append(file)
                    NumMseedFiles += 1

                # build empty time_list to fill later with
                # GetStartEnd
                timeid = ":".join(
                    list(map(str, (stat, loc, net))),)
                self.TraceTimeDict[timeid] = []
                msfile.close()

        self.ActiveText = ""
        self.FoundFiles += NumMseedFiles
        self.odd_file_list = []
        self.clearAll()

########################################

    class TopLevel(QWidget):
        """
        Class to handle (most) child windows
        """
        def __init__(self, TLtype, parent):
            """
            Child top level window
            """
            super().__init__()
            self.setAttribute(Qt.WidgetAttribute.WA_DeleteOnClose, True)
            self.parent = parent
            if TLtype == "List Traces":
                self.showTraces()
            elif TLtype == "Template Format":
                self.showTemplate()
            elif TLtype == "templ" or TLtype == "log":
                self.showSave(TLtype)
            elif TLtype == "odd_file":
                self.showFixOddFiles()

        def showFixOddFiles(self):
            """
            Show user selectable table of mseeds with
            variable record length to fix
            """
            file_list = self.parent.odd_file_list
            self.resize(460, 500)
            self.setWindowTitle("Fix Record Length")
            self.setWindowModality(Qt.WindowModality.ApplicationModal)

            self.table = QTableWidget()
            self.table.setColumnCount(4)
            self.table.setHorizontalHeaderLabels(
                ['Stat', 'Chan', 'Loc', 'Net'])
            self.table.setEditTriggers(
                QTableWidget.EditTrigger.NoEditTriggers)
            self.table.setSelectionBehavior(
                QTableWidget.SelectionBehavior.SelectRows)
            self.table.setSelectionMode(
                QTableWidget.SelectionMode.ExtendedSelection)

            ok_btn = QPushButton("Accept")
            ok_btn.clicked.connect(self.getFilesToFix)
            text = "Scan traces found mseeds with variable record length.\n"
            text += "To modify them, their record lengths must be fixed. "
            text += "Please choose which files to fix."
            label = QLabel(text)
            select_all_cb = QCheckBox("Select All")
            select_all_cb.clicked.connect(
                lambda: self.table.selectAll() if select_all_cb.isChecked()
                else self.table.clearSelection()
            )

            layout = QVBoxLayout(self)
            layout.addWidget(label)
            layout.addWidget(select_all_cb)
            layout.addWidget(self.table)
            layout.addWidget(ok_btn)

            for row, file in enumerate(file_list):
                self.table.insertRow(row)
                msfile = Mseed(file)
                stat = msfile.FH.Stat.decode().strip()
                chan = msfile.FH.Chan.decode().strip()
                loc = msfile.FH.Loc.decode().strip()
                net = msfile.FH.Net.decode().strip()

                stat_item = QTableWidgetItem(stat)
                stat_item.setData(Qt.ItemDataRole.UserRole, file)

                self.table.setItem(row, 0, stat_item)
                self.table.setItem(row, 1, QTableWidgetItem(chan))
                self.table.setItem(row, 2, QTableWidgetItem(loc))
                self.table.setItem(row, 3, QTableWidgetItem(net))

            self.parent.odd_file_list = []

        def getFilesToFix(self):
            """
            Get selected mseeds from the table
            """
            for item in self.table.selectedItems():
                # retrieve filename
                if item.column() == 0:
                    filename = item.data(Qt.ItemDataRole.UserRole)
                    self.parent.odd_file_list.append(filename)
            self.parent.launchFixRecordLength()
            # self.parent.buildTrcList()
            self.close()

        def showTraces(self):
            """
            List traces for a given key
            """
            traces = self.parent.TraceDict[
                self.parent.StationName_cb.currentText()]
            cnt = len(traces)
            traces.sort()

            self.resize(600, 300)
            self.setWindowTitle("Trace Listing: %d files listed" % cnt)

            traceText = QTextEdit()
            dismiss_b = QPushButton("Dismiss")
            dismiss_b.clicked.connect(lambda: self.close())
            dismiss_b.setStyleSheet(
                "QPushButton{background-color:lightblue;}"
                "QPushButton::hover{background-color:red;}")

            # fill trace text
            bgcolor = "white"
            for trc in traces:
                traceText.setTextBackgroundColor(bgcolor)
                traceText.append(trc + "\t\t")
                if bgcolor == "white":
                    bgcolor = "lightblue"
                else:
                    bgcolor = "white"
            # set cursor to beginning
            traceText.moveCursor(QTextCursor.Start, QTextCursor.MoveAnchor)

            layout = QVBoxLayout(self)
            layout.setContentsMargins(0, 0, 0, 0)
            layout.addWidget(traceText)
            layout.addWidget(dismiss_b)

        def showTemplate(self):
            """
            Show file template and examples
            """
            self.resize(800, 300)
            self.setWindowTitle("Template Format")

            formatText = QTextEdit()
            exit_b = QPushButton("Done")
            exit_b.setStyleSheet("""
                QPushButton{background-color:lightblue;}
                QPushButton::hover{background-color:red;}""")
            exit_b.clicked.connect(lambda: self.close())
            layout = QVBoxLayout(self)
            layout.setContentsMargins(0, 0, 0, 0)
            layout.addWidget(formatText)
            layout.addWidget(exit_b)

            # colors for text
            Blue = QColor(0, 0, 153)
            Red = QColor(136, 8, 8)
            Black = QColor(0, 0, 0)

            # fill textbox
            formatText.setTextColor(Black)
            formatText.insertPlainText(
                """The Template file consists of two lists: """)
            formatText.setTextColor(Red)
            formatText.insertPlainText("hdrlist")
            formatText.setTextColor(Black)
            formatText.insertPlainText(" and ")
            formatText.setTextColor(Red)
            formatText.insertPlainText("timelist.\n")
            formatText.setTextColor(Black)
            formatText.insertPlainText(
                "These lists are delimitted by '{}'. Comments are allowed\
                      in the template file\n")
            formatText.insertPlainText(
                "and are denoted by a '#' in the first column.\n\n")
            formatText.insertPlainText("For the")
            formatText.setTextColor(Red)
            formatText.insertPlainText(" headerlist ")
            formatText.setTextColor(Black)
            formatText.insertPlainText(
                "the columns are:\nstat:chan:loc:net:sps\tstat:chan:loc:net\n")
            formatText.insertPlainText("\nFor the ")
            formatText.setTextColor(Red)
            formatText.insertPlainText(" timelist ")
            formatText.setTextColor(Black)
            formatText.insertPlainText(
                "the columns are:\nsta:loc:net\tStart_Time\t\tEnd_Time\
                    \t\tShift(s)\tTime_Tag\tCorr_Type\n")
            formatText.insertPlainText(
                "sta:loc:net\tyyyy:ddd:hh:mm:ss\tyyyy:ddd:hh:mm:ss\t\
                    float/NA\tset/unset/NA\tadd/replace/NA")
            formatText.insertPlainText("\n\nEXAMPLE:\n")
            formatText.setTextColor(Blue)
            formatText.insertPlainText("""
            # Header Changes
            # stat:chan:loc:net:sps   stat:chan:loc:net
            hdrlist{
                01053:1C4::XX:40.0      SITE:BHZ::PI
                01053:1C5::XX:40.0      SITE:BHN::PI
                01053:1C6::XX:40.0      SITE:BHE::PI
            }
            # Timing Corrections
            # sta:loc:net\tStart_Time\t\tEnd_Time\t\tShift(s)\tTime_Tag\tCorr_Type
            # sta:loc:net\tyyyy:ddd:hh:mm:ss\tyyyy:ddd:hh:mm:ss\tfloat/NA\tset/unset/NA\tadd/replace/NA
            timelist{
                9294::SS\t2005:304:00:00:00\t2005:305:22:15:10\t0.56\tset\tadd
            }
            """)

            # set cursor to beginning
            formatText.moveCursor(QTextCursor.Start, QTextCursor.MoveAnchor)

        def showSave(self, saveType):
            """
            Populate save window
            """
            # set window size
            self.resize(250, 100)
            # setup output file name
            (year, month, day, hour, minute, second, weekday, yearday,
                daylight) = time.localtime(time.time())
            now = ".".join(list(map(str, (year, yearday, hour, minute))), )

            if saveType == "templ":
                savefile = "fixhdr_tmpl." + now
                title = "Save Template File As:"

            # log
            else:
                if self.parent.All.isChecked():
                    savefile = "fixhdr_LogAll." + now
                elif self.parent.Header.isChecked():
                    savefile = "fixhdr_LogHeader." + now
                elif self.parent.Timing.isChecked():
                    savefile = "fixhdr_LogTime." + now
                elif self.parent.Endian.isChecked():
                    savefile = "fixhdr_LogEndian." + now
                title = "Save Log File As:"

            save_le = QLineEdit(savefile)
            save_b = QPushButton("Save")
            save_b.clicked.connect(
                lambda: self.writeFile(save_le.text(), saveType))
            save_b.setStyleSheet(
                "QPushButton{background-color:lightblue;}"
                "QPushButton::hover{background-color:green;}")
            cancel_b = QPushButton("Cancel")
            cancel_b.clicked.connect(lambda: self.close())
            cancel_b.setStyleSheet(
                "QPushButton::hover{background-color:red;}")

            self.setWindowTitle(title)
            layout = QVBoxLayout(self)
            layout.setContentsMargins(0, 0, 0, 0)
            layout.addWidget(save_le)
            layout.addWidget(save_b)
            layout.addWidget(cancel_b)

        def writeFile(self, file, saveType):
            """
            Write File to disk
            """
            if os.path.isfile(file):
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Critical)
                msg.setWindowTitle("WARNING")
                msg.setText("This file exists!")
                msg.addButton('Overwrite', QMessageBox.YesRole)
                msg.addButton('Cancel', QMessageBox.NoRole)
                result = msg.exec_()

                # cancel
                if result == 1:
                    self.parent.addTextInfoBar()
                    self.close()
                    return

            parent = self.parent
            if saveType == "templ":
                try:
                    # make sure current entries are included
                    parent.updateHdr(parent.StationName_cb.currentText())
                    if parent.UpdateHdrDict or parent.UpdateTimeDict:
                        outfile = open(file, "w")
                        if parent.UpdateHdrDict:
                            outfile.write("%s\n" % "# Header Changes")
                            outfile.write(
                                "%s\n" %
                                "# stat:chan:loc:net:sps   stat:chan:loc:net")
                            outfile.write("%s\n" % "hdrlist{")
                            inlist = sorted(list(parent.UpdateHdrDict.keys()))

                            for key in inlist:
                                if "Station_Name" in parent.UpdateHdrDict[key]:
                                    newkey = \
                                        (parent.
                                         UpdateHdrDict[key]["Station_Name"])
                                else:
                                    newkey = ""
                                if "Channel" in parent.UpdateHdrDict[key]:
                                    newkey = newkey + ":" + \
                                        parent.UpdateHdrDict[key]["Channel"]
                                else:
                                    newkey = newkey + ":"
                                if "Location_Code" in \
                                        parent.UpdateHdrDict[key]:
                                    newkey = newkey + ":" + \
                                        (parent.
                                         UpdateHdrDict[key]["Location_Code"])
                                else:
                                    newkey = newkey + ":"
                                if "Network_Code" in parent.UpdateHdrDict[key]:
                                    newkey = newkey + ":" + \
                                        (parent.
                                         UpdateHdrDict[key]["Network_Code"])
                                else:
                                    newkey = newkey + ":"
                                outfile.write("%s\t%s\n" % (key, newkey))
                            outfile.write("%s\n" % "}")
                        if parent.UpdateTimeDict:
                            outfile.write("%s\n" % "# Timing Corrections")
                            outfile.write(
                                "%s\n" % "# sta:loc:net Start_Time\t"
                                "End_Time\tTime_Shift_sec Time_Tag_Quest"
                                "  Corr_Type")
                            outfile.write(
                                "%s\n" % "# sta:loc:net yyyy:ddd:hh:mm:ss "
                                "yyyy:ddd:hh:mm:ss float/NA       "
                                "set/unset/NA    add/replace/NA")

                            outfile.write("%s\n" % "timelist{")
                            inlist = sorted(list(parent.UpdateTimeDict.keys()))
                            for key in inlist:
                                timekey = key
                                for var in parent.ShiftVars:
                                    if var[0] in parent.UpdateTimeDict[key]:
                                        if var[0] == "Corr_Type":
                                            if (parent.
                                                    UpdateTimeDict
                                                    [key][var[0]]):
                                                timekey = timekey + "\treplace"
                                            else:
                                                timekey = timekey + "\tadd"
                                        elif not parent.\
                                                UpdateTimeDict[key][var[0]]:
                                            timekey = timekey + "\tNA"
                                        else:
                                            timekey = timekey + "\t" + \
                                                str(parent.UpdateTimeDict[
                                                    key][var[0]])
                                    else:
                                        timekey = timekey + "\tNA"
                                outfile.write("%s\n" % (timekey))
                            outfile.write("%s\n" % "}")
                        outfile.close()

                        # Write some log info
                        if parent.UpdateHdrDict:
                            parent.writeLog(
                                "\n<Save Template>", "Header", "blue")
                            parent.writeLog(
                                "File: %s" % file, "Header", "dkgreen")
                        if parent.UpdateTimeDict:
                            parent.writeLog(
                                "\n<Save Template>", "Time", "blue")
                            parent.writeLog(
                                "File: %s" % file, "Time", "dkgreen")
                        if parent.UpdateHdrDict or parent.UpdateTimeDict:
                            parent.dumpUpdateDicts()
                        if parent.UpdateHdrDict:
                            parent.writeLog(
                                "<end>", "Header", "blue")
                        if parent.UpdateTimeDict:
                            parent.writeLog(
                                "<end>", "Time", "blue")

                        parent.addTextInfoBar(
                            "Wrote Template File: %s " % file, 'green')
                    else:
                        err = "Dictionaries are empty. Nothing to save!"
                        parent.addTextInfoBar(err, 'orange')
                except IOError as e:
                    err = "Can't Create %s" % file, e
                    parent.addTextInfoBar(err, 'red')
            # log
            else:
                if len(parent.LogStack.widget(0).toPlainText()) == 0:
                    err = "LogText is empty, nothing to save!"
                    parent.addTextInfoBar(err, 'orange')
                else:
                    try:
                        file = open(file, 'w')
                        text = parent.LogStack.widget(0).toPlainText()
                        file.write(text)
                        file.close()
                        parent.writeLog(
                            "\n<Save Log>", "All", "blue")
                        parent.writeLog(
                            "File: %s" % file.name, "All", "green")
                        parent.writeLog(
                            "<end>", "All", "blue")
                        parent.addTextInfoBar(
                            "Wrote Log File: %s " % file.name, 'green')
                    except IOError as e:
                        err = "Can't Create %s" % file.name, e
                        parent.addTextInfoBar(err, 'red')
            self.close()

    def initTimeCorWin(self):
        """
        Create List Time Corrections window
        """
        # populate with base widgets
        self.timeCorWin = QWidget()
        self.timeCorWin.setAttribute(Qt.WA_DeleteOnClose, True)
        label = QLabel("Time Corrections Found in File Headers")
        label.setAlignment(Qt.AlignCenter)

        # QTreeView to hold time cor info;
        # QTextEdit couldn't handle the size
        # of larger data sets
        self.timeCorWin.view = QTreeView()
        self.timeCorWin.view.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.timeCorWin.view.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.timeCorWin.model = QStandardItemModel()
        self.timeCorWin.model.setHorizontalHeaderLabels(
            ["", "Block Start Time",
             "Cor(s)", "Status",
             "Timing Questionable"])
        self.timeCorWin.view.setModel(self.timeCorWin.model)
        self.timeCorWin.view.setUniformRowHeights(True)

        exit_b = QPushButton("Dismiss")
        exit_b.clicked.connect(
            self.closeTimeCorWin)
        exit_b.setStyleSheet(
            "QPushButton{background-color:lightblue;}"
            "QPushButton::hover{background-color:red;}")
        layout = QVBoxLayout(self.timeCorWin)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(label)
        layout.addWidget(self.timeCorWin.view)
        layout.addWidget(exit_b)
        self.timeCorWin.resize(600, 300)
        self.timeCorWin.setWindowTitle("Time Corrections")

    def closeTimeCorWin(self):
        """
        Close List Time Corrections window
        """
        self.timeCorWin.close()

    def updateLTC(self, row, c):
        """
        Update List Time Cor Window from main
        thread when worker thread emits
        update_LTC signal
        """
        # set bg color for trace
        if c:
            color = Qt.white
        else:
            color = Qt.cyan

        # populate QTreeView
        parent = QStandardItem(row[0])
        parent.setBackground(QBrush(color))
        row.pop(0)
        for arr in row:
            empty = QStandardItem('')
            empty.setBackground(QBrush(color))
            first = QStandardItem(str(arr[0]))
            first.setBackground(QBrush(color))
            second = QStandardItem(str(arr[1]))
            second.setBackground(QBrush(color))
            third = QStandardItem(str(arr[2]))
            third.setBackground(QBrush(color))
            fourth = QStandardItem(str(arr[3]))
            fourth.setBackground(QBrush(color))
            parent.appendRow([
                empty, first,
                second, third,
                fourth])
        self.timeCorWin.model.appendRow(parent)

    def afterUpdateTLC(self):
        """
        Update trace container text to span
        over all columns after thread is done
        """
        for i in range(self.timeCorWin.model.rowCount()):
            self.timeCorWin.view.setFirstColumnSpanned(
                i, self.timeCorWin.view.rootIndex(), True)

    def updateTimeCorWin(self):
        """
        Update List Time Corrections window
        based on TimeDict
        """

        # tracekey = self.TimeShiftName_cb.currentText()
        TimeDict = self.getTimeCor()
        c = 0
        tracelist = sorted(list(TimeDict.keys()))
        cnt = 0
        self.funcWorker.update.emit(cnt)
        for key in tracelist:
            # handle cancel request
            if not self.RunListTimeCor:
                break
            if c:
                row = []
                row.append(key)
                for tup in TimeDict[key]:
                    child_row = []
                    # handle cancel request
                    if not self.RunListTimeCor:
                        break
                    for i in range(len(tup)):
                        child_row.append(str(tup[i]))
                    row.append(child_row)
                    # bc this loop and the one below
                    # can take a while
                    # go ahead and update main thread
                    self.funcWorker.update.emit(cnt)
                c = 0
                self.funcWorker.update_LTC.emit(row, c)
            else:
                row = []
                row.append(key)
                for tup in TimeDict[key]:
                    child_row = []
                    # handle interrupt request
                    if not self.RunListTimeCor:
                        break
                    for i in range(len(tup)):
                        child_row.append(str(tup[i]))
                    row.append(child_row)
                    self.funcWorker.update.emit(cnt)
                c += 1
                self.funcWorker.update_LTC.emit(row, c)

            cnt += 1
            self.funcWorker.update.emit(cnt)

########################################

    def initThread(self, title, func, afterFunc=''):
        """
        Create thread to run function
        """
        # init thread and worker
        self.thread = QThread(self)
        self.funcWorker = Worker(func)

        # set signals/slots
        self.funcWorker.update.connect(self.updateProg)
        self.thread.started.connect(self.funcWorker.run)

        # function to run after thread finishes
        if afterFunc:
            self.funcWorker.finished.connect(afterFunc)

        # delete worker and thread when done
        self.funcWorker.finished.connect(self.thread.quit)
        self.funcWorker.finished.connect(self.funcWorker.deleteLater)
        self.thread.finished.connect(self.thread.deleteLater)

        # init vars related to progress window
        if not BATCHMODE:
            # get file num for progress window
            numFiles = 0

            if title == "List Time Corrections":
                numFiles = self.timeCnt
                self.funcWorker.update_LTC.connect(self.updateLTC)
                self.funcWorker.finished.connect(self.afterUpdateTLC)
                self.funcWorker.finished.connect(self.timeCorWin.show)
            elif title == "Build Trace db":
                for dir in self.DataDirList:
                    numFiles += (
                        sum([len(files) for r, d, files in os.walk(dir)]))
            elif title == "Modify Headers":
                inlist = sorted(list(self.UpdateHdrDict.keys()))
                for key in inlist:
                    ifilelist = (ifile for ifile in self.TraceDict[key])
                    numFiles += (sum(1 for _ in ifilelist))
            elif title == "Apply Time Corrections":
                numFiles = self.timeCnt
            elif title == "Change Endian To Little":
                numFiles = len(self.BigEndianList)
            elif title == "Change Endian To Big":
                numFiles = len(self.LittleEndianList)
            elif title == "Fixing Record Lenghth & Updating Trace db":
                numFiles = len(self.odd_file_list)

            # launch progress window
            self.initProgWindow(title, numFiles)
            self.funcWorker.finished.connect(
                lambda: self.progWin.done(0))

            # give progress window a small
            # amount of time to finish
            # loading before
            # starting thread
            loop = QEventLoop(self)
            QTimer.singleShot(100, loop.quit)
            loop.exec_()

        self.thread.start()

########################################

    def initProgWindow(self, title, numFiles):
        """
        Create progress dialog to update user of
        current process status
        """
        text = title + " is Active. Please wait."
        self.progWin = QProgressDialog(
            labelText=text, minimum=0,
            maximum=numFiles, parent=self)

        cancel_b = QPushButton("Cancel")
        cancel_b.setStyleSheet(
            "QPushButton::hover{background-color:red;}")
        cancel_b.clicked.connect(lambda: self.cancelFunc(title))
        self.progWin.setCancelButton(cancel_b)
        self.progWin.open()

########################################

    def cancelFunc(self, title):
        """
        If user cancels current process
        set loop var to 0
        """
        if title == "Build Trace db":
            self.RunBuilddb = 0
        elif title == "Modify Headers":
            self.RunModHdrs = 0
        elif title == "Apply Time Corrections":
            self.RunApplyTimeCor = 0
        elif title == "Change Endian":
            self.RunChangeEndian = 0
        elif title == "Fixing Record Lenghth & Updating Trace db":
            self.RunFixRecordLength = 0
        else:
            self.RunListTimeCor = 0

########################################

    def updateProg(self, cnt):
        """
        Update progress window
        """
        self.progWin.setValue(cnt)

########################################

    def closeEvent(self, event):
        """
        If child windows still exist, close them
        """
        QApplication.closeAllWindows()

########################################
# end of main class


class Worker(QObject):
    """
    Thread worker
    """
    update = Signal(int)  # ProgWin progressbar counter
    new_max = Signal(int)  # ProgWin progressbar max
    update_LTC = Signal(list, int)
    finished = Signal()

    def __init__(self, func):
        super().__init__()
        self.func = func

    def run(self):
        self.func()
        self.finished.emit()


def main():
    app = QApplication()
    app.setStyle('Fusion')
    if BatchFile:
        mw = MainWindow("fixhdr %s" % VERSION, BatchFile)
    else:
        mw = MainWindow("fixhdr %s" % VERSION)
        mw.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
