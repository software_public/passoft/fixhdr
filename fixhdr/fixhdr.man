.TH fixhdr l 2007.192 "" "PASSCAL MANUAL PAGES"
.SH NAME
\fBfixhdr\fR - a GUI for modifying mseed file header fields
('Station Name', 'Channel', 'Location Code', and 'Network Code'),
to apply time shifts and to convert header endianess.
.SH SYNOPSIS
\fBfixhdr\fR
.br
\fBfixhdr -#\fR
.br
\fBfixhdr -h\fR
.br
\fBfixhdr  [-d DataDirs] [-m batch_file] [-t batch_file] [-e endianess]\fR
.SH OPTIONS
\fB-#\fR returns version number
.br
\fB-h \fR returns usage
.br
\fB-d DataDirs\fR colon separated list of data directories [default: cwd]
.br
\fB-m batch_file\fR correct mseed headers in batch mode using batch_file
.br
\fB-t batch_file\fR correct trace timing in batch mode using batch_file
.br
\fB-e endianess\fR convert trace headers to endianess [big or little]
.br
\fB NOTE: -m, -t & -e are mutually exclusive. If you wish to do
both timing and headers, run time corrections first.\fR
.SH DESCRIPTION
\fBfixhdr\fR
has seven notebooks: [Trace Headers], [Global Modify], [Time Shift],
[Endianess], [Log], [Help] and [MSEED format]. [Trace Headers] and [Global Modify]
provide a means to read and modify mseed header fields (station, channel,
location code, and network code) for files found beneath a specified 
directory list. [Time Shift] allows the user to apply a bulk time shift to
traces that fall between a start and end time and to set a header flag
indicating that the time tag is questionable. [Endianess] converts traces
between little and big, or big and little endian headers. The [Log] notebook 
maintains records of key events.
.LP
.SH Root Window
.LP
.nf
Buttons:
  <Load Template>: loads a previously saved or user created mapping
    of header and timing modification that then can be applied.
  <Save Template>: saves a map of all header and timing modifications.
   Example:
   # Header Changes
   # stat:chan:loc:net:sps   stat:chan:loc:net
   hdrlist{
     01053:1C4::XX:40.0      SITE:BHZ::PI
     01053:1C5::XX:40.0      SITE:BHN::PI
     01053:1C6::XX:40.0      SITE:BHE::PI
   }
   # Timing Corrections
   # sta:loc:net Start_Time        End_Time          Shift(s) Time_Tag     Corr_Type
   # sta:loc:net yyyy:ddd:hh:mm:ss yyyy:ddd:hh:mm:ss float/NA set/unset/NA add/replace/NA
   timelist{
     9294::SS    2005:304:00:00:00 2005:305:22:15:10 0.56     set          add
   }

  <Exit>: Exits \fBfixhdr\fR and will query if not all mappings in
    Updata_Dictionary have been applied.
.fi
.LP
.SH Trace Headers
.LP
.nf
General:
  >>specify/load directories for header investigation/modifications
  >>manipulate/edit individual <sta><chn><loc><net> entries for
    loaded headers
  >>modify trace headers as specified in the "New Value" column
    for _ALL_ loaded headers
  >>store and recall header modification templates
Buttons:
  <Build Trace db>: Searchs the directories listed in the "Data-
    Directories" entry box (a colon separated list) and builds a
    list of mseed files found indexing them on unique values of
    <sta><chn><loc><net><sps>. You can narrow your search by
    entering stations in the "Find Only Stations" entry box (a
    colon separated list).
  <Find>: Launches a file browser allowing the user to add
    directories to the "Data Directories" entry box. Double
    clicking selects the new directory.
  <Clear>: Clears the "Data Directories" entry box.
  <List Traces>: Lists all traces for the selected Sta:Chan:Loc:Net:Sps.
  <Modify Headers>: Applies all current values in the 
    Update_Dictionary (that can be viewed in the [Log] notebook)
    to the current trace list that was built using <Build Trace db>
  <Clear Current>: clears all entries in the Update_Dictionary and 
    display for the currently selected sta:chan:loc:net:sps that 
    have not been applied.
  <Clear All>: clears all entries in the Update_Dictionary that
    have not been applied.
.fi
.LP
.SH Global Modify
.LP
.nf
General:
  >>specify header values in "For Headers Matching:" column, using
    drop-down menus. Wildcards are allowed and are the default.
  >>enter new header values in "Substitute Values:"
Buttons:
  <Global Set>: inserts the values in "Subsitute Values" into the "
    Update_Dictionary using For Headers Matching" entries to
    determine which <sta><chn><loc><net><sps> to modify. This only
    creates entries in the dictionary and does NOT apply them to
    the mseed headers. You must use [Trace Headers]-><Modify Headers>
     to apply these updates.
  <Clear All>: clears all current entries. This does not
    affect the Update_Dictionary.
.fi
.LP
.SH Time Shift
.LP
.nf
General:
  >> specify header values in "For Traces Matching:" column, 
     using drop-down menus. Once selected first and last sample 
     times will be displayed. The start and end time can be changed.
  >> enter time in seconds to be added to the blockette start times 
     in then "Time Shift (s):" window.
  >> "Time Tag is questionable" allows you to flip a bit in the
     Data Quality flags in the mseed file's Fixed Section of
     Data Header.
  >> "Applied" indicators notify a user when corrections have been
     appied in this instance of \fBfixhdr\fR.
Buttons:
  <How to Treat Existing Corrections>: you can choose to either 
    "Add To" to any correction listed in the Fixed Header or
    "Replace" any existing correction with the one entered.
  <Undo Time Corrections>: Allows user to un-apply previously 
     applied timing corrections. Note: Use with caution. This 
     will only remove single and cumulative corrections. 
     Review 'List Time Corrections' to better understand 
     corrections already applied to trace headers.
  <List Time Corrections>: Displays time corrections for traces
     matching the selected Stat:Loc:Net key.
  <Apply Time Corrections>: to apply corrections.
  <Time Set>: Allows user to build a dictionary of timing corrections 
     to be applied with the "Apply Time Corrections" button. i.e. 
     timing corrections for multiple Stat:Loc:Net selections can
     be set prior to applying in a single instance.
  <Recalc Start/End>: resets the start and end times from the trace 
     headers. The first time a trace is read in the start and end 
     times are cached for future use. If you change these times, 
     this button is used to update the cache and display.
  <Clear All>: clears all current entries.
.fi
.LP
.SH Endianess
.LP
.nf
General:
  >> displays the number of big and little endian files found. 
     Allows user to convert between big and little endian headers.
Buttons:
  <Convert to Big>: converts headers from little to big endian.
  <Convert to Little>: converts headers from big to little endian.
.fi
.LP
.SH Log
.LP
.nf
General:
  >>displays key events (e.g. "Build Trace db", "Modify Headers",
    etc.). Radio buttons allow you to select all log messages or
    window only those messages related to Header events, Time events,
    or changes in endianess.
Buttons:
  <Dump UpdateDict>: Dumps the current values in the Update_Dictionary
    and indicates whether or not they have been applied (i.e.
    <Modify Headers> has been run).
  <Save Log File>: Saves text window to an output file.
  <Clear Log File>: Clears text window and flushes buffer.
.fi
.SH KEYWORDS
mseed; header information; header modification
.SH SEE ALSO
SEED manual (pdf), mseedhdr
.SH AUTHOR
Bruce Beaudoin (bruce@passcal.nmt.edu)
