=======
History
=======

2009.175 (2018-06-07)
------------------

* First release on new build system.

2018.176 (2018-06-25)
-------------------

* Updating version numbers to 2018.176.

2020.212 (2020-07-30)
------------------
* Updated to work with Python 3
* Added a unit test to test fixhdr import
* Updated list of platform specific dependencies to be installed when pip
  installing fixhdr (see setup.py)
* Installed and tested fixhdr against Python3.[6,7,8] using tox
* Formatted Python code to conform to the PEP8 style guide
* Created conda packages for fixhdr that can run on Python3.[6,7,8]
* Updated .gitlab-ci.yml to run a linter and unit tests for Python3.[6,7,8]
  in GitLab CI pipeline
