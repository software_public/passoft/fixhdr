#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `fixhdr` package."""

import unittest

from unittest.mock import patch


class TestFixhdr(unittest.TestCase):
    """Tests for `fixhdr` package."""

    def test_import(self):
        """Test fixhdr import"""
        with patch("sys.argv", ["fixhdr", "-#"]):
            with self.assertRaises(SystemExit) as cmd:
                try:
                    import fixhdr.fixhdr as fx
                    fx.Usage()
                except ImportError as e:
                    print(e)
                    self.fail("fixhdr import failed")
            self.assertEqual(cmd.exception.code, 0, "sys.exit(0) never called "
                             "- Failed to exercise fixhdr")
